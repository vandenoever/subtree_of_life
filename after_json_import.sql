# run this script after running
#  lbzcat latest-all.json.bz2 | sol parse
# and
#  < ott3.3/taxonomy.tsv sol parse_ott

# command for this script:
#    < after_json_import.sql sqlite3 -batch -bail -echo db/life.sqlite
PRAGMA journal_mode = OFF;
PRAGMA synchronous = OFF;
PRAGMA temp_store = MEMORY;
PRAGMA cache_size = -64000;
PRAGMA mmap_size = 300000000000;

ATTACH 'file:db/wikidata_json.sqlite?mode=ro' AS json;

DROP TABLE IF EXISTS ott_id;
CREATE TABLE ott_id (
    ott_id INTEGER PRIMARY KEY,
    id     INTEGER NOT NULL
);

CREATE TEMP TABLE import(id, ott_id, comment);
.separator ,
.import manual_links.csv import

INSERT INTO ott_id SELECT ott_id, id FROM import;

DROP TABLE import;

SELECT count(*) FROM ott_id;

INSERT INTO ott_id
     SELECT o.id, c.id
       FROM json.claim c
       JOIN ott_taxon o ON c.value = o.id
  LEFT JOIN ott_id i ON o.id = i.ott_id
      WHERE c.claim = 9157 AND i.id IS NULL;

SELECT count(*) FROM ott_id;

DROP TABLE IF EXISTS ott_wiki;
CREATE TABLE ott_wiki(
    ott_id INTEGER PRIMARY KEY,
    id     INTEGER NOT NULL
);

INSERT INTO ott_wiki
  SELECT g.ott ott_id, c.id
    FROM json.claim c
    JOIN ott_gbif g ON c.value IS g.gbif
   WHERE c.claim = 846
GROUP BY ott_id;
SELECT count(*) FROM ott_wiki;

INSERT INTO ott_id
   SELECT g.ott_id, g.id
     FROM ott_wiki g
LEFT JOIN ott_id i ON g.ott_id = i.ott_id
    WHERE i.ott_id IS NULL;

SELECT count(*) FROM ott_id;

DELETE FROM ott_wiki;

INSERT INTO ott_wiki
  SELECT g.ott ott_id, c.id
    FROM json.claim c
    JOIN ott_irmng g ON c.value IS g.irmng
   WHERE c.claim = 5055
GROUP BY ott_id;
SELECT count(*) FROM ott_wiki;

 INSERT INTO ott_id
   SELECT g.ott_id, g.id
     FROM ott_wiki g
LEFT JOIN ott_id i ON g.ott_id = i.ott_id
    WHERE i.ott_id IS NULL;

DROP TABLE ott_wiki;

SELECT count(*) FROM ott_id;

# this could also use the alias field but that is currently disabled
   CREATE TEMP TABLE label_matches AS
   SELECT o.id ott_id, l.id id, count(*) count, o.label
     FROM ott_label o
LEFT JOIN ott_id i ON o.id = i.ott_id
     JOIN label l ON o.label COLLATE NOCASE = l.label COLLATE NOCASE
    WHERE i.ott_id IS NULL
 GROUP BY l.id, o.id
   HAVING count > 5 OR o.label LIKE '% %';

CREATE INDEX label_matches_index ON label_matches(ott_id, count);

SELECT count(*) FROM label_matches;

INSERT INTO ott_id
     SELECT l1.ott_id, l1.id
       FROM label_matches l1
  LEFT JOIN label_matches l2
         ON l1.ott_id = l2.ott_id AND l1.count < l2.count
      WHERE l2.ott_id IS NULL
   GROUP BY l1.ott_id;

SELECT count(*) FROM ott_id;

DROP TABLE label_matches;

DROP TABLE IF EXISTS data;
CREATE TABLE data (
    id INTEGER PRIMARY KEY,
    rank TEXT,
    wikidata_id INTEGER,
    wiki_images TEXT,
    year INTEGER
);

INSERT INTO data
   SELECT ott_taxon.id,
          ott_label.rank,
          ott_id.id AS wikidata_id,
          wiki_images,
          year
     FROM ott_taxon
LEFT JOIN ott_label ON ott_taxon.id = ott_label.id AND rank NOT LIKE 'no rank%'
LEFT JOIN ott_id ON ott_id.ott_id = ott_taxon.id
LEFT JOIN (
          SELECT id, group_concat(value, char(9)) wiki_images
          FROM claim
          WHERE claim = 18
          GROUP BY id
          ) AS wiki_images ON wiki_images.id = ott_id.id
LEFT JOIN (
          SELECT id, CAST(json_extract(value, '$.time') AS INTEGER) year FROM claim
          WHERE claim = 580
          ) AS year ON year.id = ott_id.id;

DROP TABLE IF EXISTS name_unsorted;
CREATE TABLE name_unsorted (
    id INTEGER NOT NULL,
    language TEXT,
    label TEXT,
    alias TEXT,
    common_name TEXT,
    wiki_link TEXT,
    default_wiki_link INTEGER,
    UNIQUE(id, language)
);

DROP TABLE IF EXISTS name;
CREATE TABLE name (
    id INTEGER NOT NULL,
    language TEXT,
    label TEXT,
    alias TEXT,
    common_name TEXT,
    wiki_link TEXT,
    default_wiki_link INTEGER,
    UNIQUE(id, language)
);

INSERT INTO name_unsorted
SELECT id, NULL, label, NULL, NULL, NULL, NULL
  FROM ott_label;

DROP TABLE IF EXISTS alias_dedup;
CREATE TABLE alias_dedup AS
  SELECT ott_id.ott_id AS id, a.language, a.alias
    FROM json.alias a
    JOIN ott_id ON a.id = ott_id.id
    JOIN ott_label l ON ott_id.ott_id = l.id
   WHERE a.alias COLLATE NOCASE IS NOT l.label COLLATE NOCASE
GROUP BY ott_id.ott_id, a.language, a.alias COLLATE NOCASE;

DROP TABLE ott_label;

INSERT INTO name_unsorted
   SELECT ott_id.ott_id AS id,
          l.language,
          nullif(l.label COLLATE NOCASE, n.label COLLATE NOCASE) AS name,
          a.alias,
          c.common_name,
          nullif(s.link COLLATE NOCASE, n.label COLLATE NOCASE) AS wiki_link,
          iif(s.link COLLATE NOCASE = n.label COLLATE NOCASE, true, NULL) AS default_wiki_link
     FROM ott_id
     JOIN json.label l ON l.id = ott_id.id
     JOIN name_unsorted n ON n.id = ott_id.ott_id AND n.language IS NULL
LEFT JOIN (
          SELECT id, language, group_concat(alias, char(9)) alias
          FROM alias_dedup
          GROUP BY id, language
          ) AS a ON a.id = ott_id.id AND a.language = l.language
LEFT JOIN (
          SELECT id,
          json_extract(value, '$.language') AS language,
          group_concat(json_extract(value, '$.text'), char(9)) common_name
          FROM claim
          WHERE claim = 1843
          GROUP BY id, language
          ) AS c ON c.id = ott_id.id AND c.language = l.language
LEFT JOIN sitelink s ON s.id = ott_id.id AND s.wiki = l.language
    WHERE name IS NOT NULL OR a.alias IS NOT NULL OR c.common_name IS NOT NULL OR s.link IS NOT NULL OR default_wiki_link IS NOT NULL;

DROP TABLE alias_dedup;
DROP TABLE ott_id;

INSERT INTO name
  SELECT id, language, label, alias, common_name, wiki_link, default_wiki_link
    FROM name_unsorted
ORDER BY id, language;

DROP TABLE name_unsorted;

DROP TABLE IF EXISTS name_search;
CREATE VIRTUAL TABLE name_search USING fts5(
    label,
    alias,
    common_name,
    wiki_link,
    content='name',
    content_rowid='rowid'
);

INSERT INTO name_search(rowid, label, alias, common_name, wiki_link) SELECT rowid, label, alias, common_name, wiki_link FROM name;

VACUUM main;
ANALYZE main;
