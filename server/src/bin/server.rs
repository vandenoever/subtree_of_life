use askama::Template;
use axum::{
    extract::{self, Path, Query},
    http::{self, header, HeaderMap, HeaderValue, StatusCode},
    response::{IntoResponse, Response},
    routing::get,
    Router,
};
use fluent::{FluentBundle, FluentResource};
use serde::{Deserialize, Deserializer};
use std::{
    borrow::Cow,
    collections::{BTreeMap, BTreeSet, HashMap, HashSet},
    error::Error,
    fmt::{Display, Formatter, Write},
    fs::{File, OpenOptions},
    net::SocketAddr,
    path::PathBuf,
    str::FromStr,
    sync::Arc,
};
use subtree_of_life::{load_info, search_db, simple_graph_data, Graph, Sol, TaxonInfo};
use subtree_of_life_server::{
    table_tree::{create_table_tree, TableTreeRow},
    wiki_images::WikiImages,
};
use tokio::fs::ReadDir;
use tower_http::trace::TraceLayer;
use tracing_subscriber::{
    filter::LevelFilter, layer::SubscriberExt, util::SubscriberInitExt, Layer,
};
use unic_langid::LanguageIdentifier;

// parameters that comes in via GET requests
#[derive(Deserialize, Clone)]
struct Params {
    #[serde(default)]
    search: String,
    #[serde(default)]
    #[serde(deserialize_with = "string_to_uids")]
    uids: BTreeSet<usize>,
    #[serde(default)]
    #[serde(deserialize_with = "deserialize_languages")]
    lang: Languages,
}

impl Params {
    fn query(&self) -> String {
        let mut s = String::new();
        let mut empty = true;
        if !self.lang.is_empty() {
            empty = false;
            s.push_str("?lang=");
            s.push_str(&self.lang.to_string());
        }
        if !self.uids.is_empty() {
            if empty {
                s.push('?');
            } else {
                s.push('&');
            }
            empty = false;
            s.push_str("uids=");
            s.push_str(&uids_string(self.uids.iter()));
        }
        if !self.search.is_empty() {
            if empty {
                s.push('?');
            } else {
                s.push('&');
            }
            s.push_str("&search=");
            s.push_str(&self.search);
        }
        s
    }
    fn lang_query(&self, lang: &str) -> String {
        let lang = match lang.parse::<LanguageIdentifier>() {
            Err(_) => return self.query(),
            Ok(lang) => lang,
        };
        if Some(&lang) == self.lang.0.first() {
            return self.query();
        }
        let mut q = (*self).clone();
        q.lang.0.retain(|l| *l != lang);
        q.lang.0.insert(0, lang);
        q.query()
    }
    fn add_taxon_query(&self, uid: &usize) -> String {
        let mut q = self.clone();
        q.uids.insert(*uid);
        q.search.clear();
        q.query()
    }
    fn remove_taxon_query(&self, uid: &usize, graph: &Graph) -> String {
        let mut q = self.clone();
        q.uids.remove(uid);
        remove_children(&mut q.uids, *uid, graph);
        q.query()
    }
    fn uids(&self) -> String {
        uids_string(self.uids.iter())
    }
}

fn uids_string<'a>(uids: impl Iterator<Item = &'a usize>) -> String {
    let mut uids = uids.fold(String::new(), |mut s, n| {
        write!(s, "{},", n).ok();
        s
    });
    uids.pop();
    uids
}

fn remove_children(uids: &mut BTreeSet<usize>, node: usize, graph: &Graph) {
    if let Some(children) = graph.get(&node) {
        for c in children {
            uids.remove(c);
            remove_children(uids, *c, graph);
        }
    }
}

#[derive(Clone, Default)]
struct Languages(Vec<LanguageIdentifier>);

impl Languages {
    fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    fn starts_with(&self, lang: &str) -> bool {
        if let Some(l) = self.0.first() {
            l.language.as_str() == lang
        } else {
            false
        }
    }
}

impl Display for Languages {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        for (i, lang) in self.0.iter().enumerate() {
            if i != 0 {
                write!(f, ",")?;
            }
            write!(f, "{}", lang.language.as_str())?;
        }
        Ok(())
    }
}

const DEFAULT_LANGUAGE: &str = "nl";

fn string_to_uids<'de, D>(d: D) -> Result<BTreeSet<usize>, D::Error>
where
    D: Deserializer<'de>,
{
    let mut uids = BTreeSet::default();
    for uid in String::deserialize(d)?.split(',') {
        if let Ok(uid) = usize::from_str(uid) {
            uids.insert(uid);
        }
    }
    Ok(uids)
}

fn deserialize_languages<'de, D>(d: D) -> Result<Languages, D::Error>
where
    D: Deserializer<'de>,
{
    Ok(string_to_languages(&String::deserialize(d)?))
}

fn string_to_languages(d: &str) -> Languages {
    let mut langs = Vec::new();
    for lang in d.split(',') {
        if let Ok(lang) = LanguageIdentifier::from_str(lang) {
            if !langs.contains(&lang) {
                langs.push(lang);
            }
        }
    }
    Languages(langs)
}

fn find_a_root(graph: &Graph) -> Option<usize> {
    let children: HashSet<usize> = graph.values().flatten().copied().collect();
    graph.keys().find(|k| !children.contains(k)).copied()
}

fn set_languages(lang: &mut Languages, headers: HeaderMap) {
    if lang.is_empty() {
        let h: &str = headers
            .get("accept-language")
            .and_then(|h| h.to_str().ok())
            .unwrap_or_default();
        let langs = string_to_languages(h);
        if langs.0.is_empty() {
            lang.0
                .push(LanguageIdentifier::from_str(DEFAULT_LANGUAGE).unwrap());
        } else {
            *lang = langs;
        }
    }
}

fn get_i18n<'a>(
    langs: &'a Languages,
    fluent_resources: &'a BTreeMap<String, FluentResource>,
) -> I18N<'a> {
    let li = langs.0.first().unwrap();
    let lang = li.language.as_str();
    let mut fluent = FluentBundle::new(langs.0.clone());
    for l in langs.0.iter().rev() {
        if let Some(res) = fluent_resources.get(l.language.as_str()) {
            fluent.add_resource_overriding(res);
        }
    }
    I18N {
        lang,
        bundle: fluent,
    }
}

async fn home_result(
    state: State,
    params: Query<Params>,
    headers: HeaderMap,
) -> Result<Response, Box<dyn Error>> {
    use std::ops::Deref;
    let mut params = params.deref().clone();
    let query = params.search.trim();
    set_languages(&mut params.lang, headers);
    let languages: Vec<_> = params.lang.0.iter().map(|l| l.language.as_str()).collect();
    let i18n = get_i18n(&params.lang, &state.fluent_resources);
    if query.is_empty() && params.uids.is_empty() {
        let path = get_static_path().join("index.html");
        let msg = std::fs::read_to_string(&path)
            .map_err(|e| format!("Cannot read '{}': {}", path.display(), e))?;
        return into_response(&StaticTemplate {
            msg,
            params: &params,
            taxon_info: Default::default(),
            i18n: &i18n,
        });
    }
    let (results, taxon_info) = if !query.is_empty() {
        let results = search_db(&state.sol, query, i18n.lang)?;
        let taxon_info = load_info(&state.sol, results.iter(), &languages)?;
        (results, taxon_info)
    } else {
        (Vec::new(), Default::default())
    };
    let (graph, mut taxon_info) = if query.is_empty() || results.is_empty() {
        let uids_vec: Vec<usize> = params.uids.iter().copied().collect();
        simple_graph_data(&state.sol, &uids_vec, &languages)?
    } else {
        (Default::default(), taxon_info)
    };
    only_one_image_per_taxon(&mut taxon_info);
    simplify_aliases(&mut taxon_info);
    let root = find_a_root(&graph);
    let table_tree = create_table_tree(root, &graph);
    let image_ids: Vec<&str> = taxon_info
        .values()
        .flat_map(|i| &i.images)
        .map(|i| i.name.as_str())
        .collect();
    let image_sizes = state.wiki_images.get_image_sizes(&image_ids)?;
    into_response(&MainTemplate {
        image_sizes: &image_sizes,
        params: &params,
        results,
        taxon_info,
        graph: &graph,
        i18n: &i18n,
        table_tree,
    })
}

fn log_err(r: Result<Response, Box<dyn Error>>) -> Response {
    match r {
        Err(e) => {
            tracing::error!("{:#?}", e);
            let res = Response::builder().status(StatusCode::INTERNAL_SERVER_ERROR);
            res.body("ERROR".into()).unwrap()
        }
        Ok(r) => r,
    }
}

async fn home(state: State, params: Query<Params>, headers: HeaderMap) -> Response {
    log_err(home_result(state, params, headers).await)
}

async fn wiki_image_result(
    state: State,
    Path((uid, image)): Path<(usize, String)>,
) -> Result<Response, Box<dyn Error>> {
    let bytes = state.wiki_images.get_image(&state.sol, uid, &image).await?;
    let mut res = Response::builder().status(StatusCode::OK);
    if let Some(headers) = res.headers_mut() {
        headers.insert(
            "cache-control",
            HeaderValue::from_static("public, max-age=31104000"),
        );
    }
    Ok(res.body(bytes.into())?)
}

async fn wiki_image(state: State, path: Path<(usize, String)>) -> Response {
    log_err(wiki_image_result(state, path).await)
}

struct StateInner {
    sol: Sol,
    wiki_images: WikiImages,
    fluent_resources: BTreeMap<String, FluentResource>,
}
type State = extract::State<Arc<StateInner>>;

#[derive(Template)]
#[template(path = "static.html")]
struct StaticTemplate<'a> {
    params: &'a Params,
    i18n: &'a I18N<'a>,
    msg: String,
    taxon_info: HashMap<usize, TaxonInfo>,
}

#[derive(Template)]
#[template(path = "main.html")]
struct MainTemplate<'a> {
    params: &'a Params,
    results: Vec<usize>,
    taxon_info: HashMap<usize, TaxonInfo>,
    i18n: &'a I18N<'a>,
    table_tree: Vec<TableTreeRow>,
    graph: &'a Graph,
    image_sizes: &'a BTreeMap<String, (usize, usize)>,
}

struct I18N<'a> {
    lang: &'a str,
    bundle: FluentBundle<&'a FluentResource>,
}

impl<'a> I18N<'a> {
    fn t(&self, msg: &'a str) -> Cow<'_, str> {
        let pattern =
            if let Some(pattern) = self.bundle.get_message(msg).and_then(|msg| msg.value()) {
                pattern
            } else {
                eprintln!("There is no message '{}' in i18n.", msg);
                return Cow::from(msg);
            };
        let mut errors = vec![];
        self.bundle.format_pattern(pattern, None, &mut errors)
    }
    fn year(&self, year: &i64) -> String {
        if *year <= -1_000_000_000 {
            format!("{} {}", year / -1_000_000_000, self.t("bya"))
        } else if *year <= -1_000_000 {
            format!("{} {}", year / -1_000_000, self.t("mya"))
        } else if *year <= -1_000 {
            format!("{} {}", year / -1_000, self.t("kya"))
        } else {
            year.to_string()
        }
    }
    fn year_long(&self, year: &i64) -> String {
        if *year <= -1_000_000_000 {
            format!("{} {}", year / -1_000_000_000, self.t("bya-long"))
        } else if *year <= -1_000_000 {
            format!("{} {}", year / -1_000_000, self.t("mya-long"))
        } else if *year <= -1_000 {
            format!("{} {}", year / -1_000, self.t("kya-long"))
        } else {
            year.to_string()
        }
    }
}

fn into_response<T: Template>(t: &T) -> Result<Response, Box<dyn Error>> {
    let body = t.render()?;
    let headers = [(
        http::header::CONTENT_TYPE,
        http::HeaderValue::from_static(T::MIME_TYPE),
    )];
    Ok((headers, body).into_response())
}

async fn static_path(Path(path): Path<String>) -> impl IntoResponse {
    log_err(static_file(&path).await)
}

fn get_static_path() -> PathBuf {
    let static_path = PathBuf::from("server/static");
    if static_path.is_dir() {
        static_path
    } else if let Some(asset_dir) = get_asset_dir() {
        asset_dir.join("static")
    } else {
        // Use static path even though it does not exist to hint where
        // the file can be placed.
        static_path
    }
}

async fn static_file(path: &str) -> Result<Response, Box<dyn Error>> {
    let path = path.trim_start_matches('/');
    let mime_type = mime_guess::from_path(path).first_or_text_plain();
    let full_path = get_static_path().join(path);
    if let Ok(contents) = tokio::fs::read(&full_path).await {
        Ok(Response::builder()
            .status(StatusCode::OK)
            .header(
                header::CONTENT_TYPE,
                HeaderValue::from_str(mime_type.as_ref())?,
            )
            .body(contents.into())?)
    } else {
        tracing::error!("Cannot read '{}'", full_path.display());
        Ok(Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(format!("Cannot read '{}'", path).into())?)
    }
}

async fn robots_txt() -> Response {
    log_err(static_file("robots.txt").await)
}

async fn favicon_ico() -> Response {
    log_err(static_file("favicon.ico").await)
}

async fn read_dir(path: impl AsRef<std::path::Path>) -> Result<ReadDir, Box<dyn Error>> {
    Ok(tokio::fs::read_dir(path)
        .await
        .map_err(|e| format!("Cannot read directory 'i18n': {}", e))?)
}

async fn load_languages(
    asset_dir: Option<&std::path::Path>,
) -> Result<BTreeMap<String, FluentResource>, Box<dyn Error>> {
    let mut map = BTreeMap::new();
    let mut i18n = PathBuf::from("i18n");
    if !i18n.is_dir() {
        if let Some(asset_dir) = asset_dir {
            i18n = asset_dir.join("i18n");
        } else {
            return Err("Cannot find 'i18n' directory.".into());
        }
    }
    let mut iter = read_dir(i18n).await?;
    while let Ok(Some(file)) = iter.next_entry().await {
        let name = file.file_name();
        let name = name.to_str().unwrap();
        let lang = name[0..2].to_string();
        let fluent = tokio::fs::read_to_string(file.path())
            .await
            .map_err(|e| format!("Cannot read {}: {}", file.path().display(), e))?;
        let fluent_resource = FluentResource::try_new(fluent).expect("Failed to parse FTL.");
        map.insert(lang, fluent_resource);
    }
    Ok(map)
}

fn open(path: &str) -> Result<File, Box<dyn Error>> {
    let file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(true)
        .open(path)
        .map_err(|e| format!("Cannot open {}: {}", path, e))?;
    Ok(file)
}

fn get_asset_dir() -> Option<PathBuf> {
    let mut args = std::env::args();
    let exe = PathBuf::from(args.next().unwrap());
    if let Some(dir) = exe
        .parent()
        .and_then(|p| p.parent())
        .map(|p| p.join("share"))
    {
        if dir.is_dir() {
            return Some(dir);
        }
    }
    None
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let file = open("log.txt")?;
    let log = tracing_subscriber::fmt::layer().with_writer(file);
    let file = open("error_log.txt")?;
    let error_log = tracing_subscriber::fmt::layer()
        .with_writer(file)
        .with_filter(LevelFilter::ERROR);
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG").unwrap_or_else(|_| "debug".into()),
        ))
        .with(log)
        .with(error_log)
        .with(tracing_subscriber::fmt::layer())
        .init();

    let asset_dir = get_asset_dir();
    if let Some(asset_dir) = &asset_dir {
        println!("install_dir {}", asset_dir.display());
    }
    let sol = Sol::new()?;
    let fluent_resources = load_languages(asset_dir.as_deref()).await?;
    let state = Arc::new(StateInner {
        sol,
        wiki_images: WikiImages::new()?,
        fluent_resources,
    });
    let app = Router::new()
        .route("/", get(home))
        .route("/images/wiki/:image_id/:image", get(wiki_image))
        .route("/static/*path", get(static_path))
        .route("/favicon.ico", get(favicon_ico))
        .route("/robots.txt", get(robots_txt))
        .with_state(state)
        .layer(TraceLayer::new_for_http());

    // run it with hyper on localhost:8001
    let addr = SocketAddr::from(([0, 0, 0, 0], 8001));
    tracing::debug!("listening on {}", addr);
    let listener = tokio::net::TcpListener::bind(addr).await?;
    axum::serve(listener, app.into_make_service()).await?;
    Ok(())
}

fn only_one_image_per_taxon(taxons: &mut HashMap<usize, TaxonInfo>) {
    for info in taxons.values_mut() {
        info.images.truncate(1);
    }
}

fn simplify_aliases(taxons: &mut HashMap<usize, TaxonInfo>) {
    for info in taxons.values_mut() {
        simplify_taxon_aliases(info);
    }
}

fn normalize_string(s: &str) -> String {
    use unicode_normalization::UnicodeNormalization;
    s.nfd()
        .filter(|c| c.is_alphabetic())
        .flat_map(|c| c.to_lowercase())
        .map(|c| if c == 'c' { 'k' } else { c })
        .collect()
}

fn divering_suffix_length(a: &str, b: &str) -> usize {
    let mut a_iter = a.chars();
    let mut b_iter = b.chars();
    loop {
        match (a_iter.next(), b_iter.next()) {
            (Some(a_char), Some(b_char)) => {
                if a_char != b_char {
                    return usize::max(a_iter.count(), b_iter.count());
                }
            }
            (None, Some(_)) => {
                return b_iter.count() + 1;
            }
            (Some(_), None) => {
                return a_iter.count() + 1;
            }
            (None, None) => return 0,
        }
    }
}

fn stem_equal(a: &str, b: &str) -> bool {
    let diff = divering_suffix_length(a, b);
    diff < 4
}

// remove duplicates from aliases, common_name, and label
fn simplify_taxon_aliases(taxon: &mut TaxonInfo) {
    let name = normalize_string(&taxon.name);
    let mut new_aliases = Vec::with_capacity(taxon.common_name.len() + taxon.alias.len() + 2);
    for common_name in &taxon.common_name {
        add_alias_if_new(common_name, &name, &mut new_aliases);
    }
    add_alias_if_new(&taxon.label, &name, &mut new_aliases);
    for alias in &taxon.alias {
        add_alias_if_new(alias, &name, &mut new_aliases);
    }
    taxon.alias = new_aliases.iter().map(|v| v.0.to_string()).collect();
}

fn add_alias_if_new<'a>(s: &'a str, name: &str, alias: &mut Vec<(&'a str, String)>) {
    if s.contains('/') || s.contains('(') || s.contains(',') {
        return;
    }
    let n = normalize_string(s);
    if !n.is_empty() && !alias.iter().any(|a| stem_equal(&a.1, &n)) && !stem_equal(name, &n) {
        alias.push((s, n));
    }
}
