use std::collections::BTreeMap;

use subtree_of_life::Graph;

use crate::table_tree::{
    get_child_type, ChildType, TableTreeCell, TableTreeCellChildren, TableTreeRow,
};

type Nodes = BTreeMap<usize, DrawTree>;

#[derive(Debug)]
struct DrawTree {
    x: f64,
    y: usize,
    parent: Option<usize>,
    thread: Option<usize>,
    r#mod: f64,
    ancestor: usize,
    change: f64,
    shift: f64,
    leftmost_sibling: Option<usize>,
    number: usize,
}

impl DrawTree {
    fn add_tree(
        nodes: &mut Nodes,
        id: usize,
        graph: &Graph,
        parent: Option<usize>,
        depth: usize,
        leftmost_sibling: Option<usize>,
        number: usize,
    ) {
        if let Some(c) = graph.get(&id) {
            let mut leftmost_sibling = None;
            for (i, c) in c.iter().enumerate() {
                DrawTree::add_tree(
                    nodes,
                    *c,
                    graph,
                    Some(id),
                    depth + 1,
                    leftmost_sibling,
                    i + 1,
                );
                if i == 0 {
                    leftmost_sibling = Some(*c);
                }
            }
        }
        let node = Self {
            x: -1.,
            y: depth,
            parent,
            thread: None,
            r#mod: 0.,
            ancestor: id,
            change: 0.,
            shift: 0.,
            leftmost_sibling,
            number,
        };
        nodes.insert(id, node);
    }
}

fn left(node: usize, graph: &Graph, nodes: &Nodes) -> Option<usize> {
    if let Some(thread) = nodes[&node].thread {
        return Some(thread);
    }
    if let Some(c) = graph.get(&node).and_then(|c| c.iter().next()) {
        return Some(*c);
    }
    None
}

fn right(node: usize, graph: &Graph, nodes: &Nodes) -> Option<usize> {
    if let Some(thread) = nodes[&node].thread {
        return Some(thread);
    }
    if let Some(c) = graph.get(&node).and_then(|c| c.iter().next_back()) {
        return Some(*c);
    }
    None
}

fn left_brother(node_id: usize, graph: &Graph, nodes: &Nodes) -> Option<usize> {
    let mut n = None;
    if let Some(parent) = nodes[&node_id].parent {
        let parent = &graph[&parent];
        for node in parent {
            if *node == node_id {
                return n;
            }
            n = Some(*node);
        }
    }
    n
}

fn buchheim(root: usize, graph: &Graph, nodes: &mut Nodes) {
    first_walk(root, graph, nodes, 1.);
    let min = second_walk(root, 0., None, graph, nodes);
    // move so that all x coordinates are positive
    if let Some(min) = min {
        if min < 0. {
            for v in nodes.values_mut() {
                v.x -= min;
            }
        }
    }
}

fn first_walk(node: usize, graph: &Graph, nodes: &mut Nodes, distance: f64) {
    let children = graph.get(&node);
    let mut x = 0.;
    let mut r#mod = 0.;
    if let Some(children) = children {
        let first = *children.iter().next().unwrap();
        let last = *children.iter().next_back().unwrap();
        let mut default_ancestor = first;
        for w in children {
            first_walk(*w, graph, nodes, distance);
            default_ancestor = apportion(*w, default_ancestor, graph, nodes, distance);
        }
        execute_shifts(node, graph, nodes);
        let midpoint = (nodes[&first].x + nodes[&last].x) / 2.;
        if let Some(w) = left_brother(node, graph, nodes) {
            x = nodes[&w].x + distance;
            r#mod = x - midpoint;
        } else {
            x = midpoint;
        }
    } else if let Some(left_brother) = left_brother(node, graph, nodes) {
        x = nodes[&left_brother].x + distance
    }
    let node = nodes.get_mut(&node).unwrap();
    node.x = x;
    node.r#mod = r#mod;
}

fn apportion(
    v: usize,
    mut default_ancestor: usize,
    graph: &Graph,
    nodes: &mut Nodes,
    distance: f64,
) -> usize {
    if let Some(w) = left_brother(v, graph, nodes) {
        // in buchheim notation:
        // i == inner; o == outer; r == right; l == left; r = +; l = -
        let mut vir = v;
        let mut vor = v;
        let mut vil = w;
        let mut vol = nodes[&v].leftmost_sibling.unwrap();
        let mut sir = nodes[&v].r#mod;
        let mut sor = sir;
        let mut sil = nodes[&vil].r#mod;
        let mut sol = nodes[&vol].r#mod;
        while let (Some(new_vil), Some(new_vir)) =
            (right(vil, graph, nodes), left(vir, graph, nodes))
        {
            vil = new_vil;
            vir = new_vir;
            vol = left(vol, graph, nodes).unwrap();
            vor = right(vor, graph, nodes).unwrap();
            let shift = (nodes[&vil].x + sil) - (nodes[&vir].x + sir) + distance;
            if shift > 0. {
                move_subtree(
                    ancestor(vil, v, default_ancestor, graph, nodes),
                    v,
                    shift,
                    nodes,
                );
                sir += shift;
                sor += shift;
            }
            sil += nodes[&vil].r#mod;
            sir += nodes[&vir].r#mod;
            sol += nodes[&vol].r#mod;
            sor += nodes[&vor].r#mod;
        }
        if let (Some(vil_right), None) = (right(vil, graph, nodes), right(vor, graph, nodes)) {
            let vor = nodes.get_mut(&vor).unwrap();
            vor.thread = Some(vil_right);
            vor.r#mod += sil - sor;
        } else {
            if let (Some(vir_left), None) = (left(vir, graph, nodes), right(vol, graph, nodes)) {
                let vol = nodes.get_mut(&vol).unwrap();
                vol.thread = Some(vir_left);
                vol.r#mod += sir - sol;
            }
            default_ancestor = v;
        }
    }
    default_ancestor
}

fn move_subtree(wl: usize, wr: usize, shift: f64, nodes: &mut Nodes) {
    let wln = &nodes[&wl];
    let wrn = &nodes[&wr];
    let subtrees = wrn.number - wln.number;
    println!(
        "{} is conflicted with {} moving {} shift {}",
        wl, wr, subtrees, shift
    );
    let wrn = nodes.get_mut(&wr).unwrap();
    wrn.change -= shift / subtrees as f64;
    wrn.shift += shift;
    wrn.x += shift;
    wrn.r#mod += shift;
    nodes.get_mut(&wl).unwrap().change += shift / subtrees as f64;
}

fn execute_shifts(node: usize, graph: &Graph, nodes: &mut Nodes) {
    let mut shift = 0.;
    let mut change = 0.;
    for w in &graph[&node] {
        let w = nodes.get_mut(w).unwrap();
        w.x += shift;
        w.r#mod += shift;
        change += w.change;
        shift += w.shift + change;
    }
}

fn ancestor(vil: usize, v: usize, default_ancestor: usize, graph: &Graph, nodes: &Nodes) -> usize {
    if let Some(parent) = nodes[&v].parent {
        let ancestor = nodes[&vil].ancestor;
        if graph[&parent].contains(&ancestor) {
            return ancestor;
        }
    }
    default_ancestor
}

fn second_walk(
    node: usize,
    mut m: f64,
    mut min: Option<f64>,
    graph: &Graph,
    nodes: &mut Nodes,
) -> Option<f64> {
    let v = nodes.get_mut(&node).unwrap();
    v.x += m;
    if min.is_none() {
        min = Some(v.x);
    } else if let Some(m) = min {
        if v.x < m {
            min = Some(v.x);
        }
    }
    m += v.r#mod;
    if let Some(children) = graph.get(&node) {
        for w in children {
            min = second_walk(*w, m, min, graph, nodes);
        }
    }
    min
}

fn add_nodes_to_rows(
    node: usize,
    child_type: ChildType,
    graph: &Graph,
    nodes: &Nodes,
    rows: &mut [TableTreeRow],
    offsets: &mut Vec<usize>,
    parent_cell: Option<usize>,
) {
    let n = &nodes[&node];
    let depth = n.y;
    let mut child_cells = None;
    if let Some(children) = graph.get(&node) {
        let n_children = children.len();
        let mut child_pos = TableTreeCellChildren {
            first: rows[depth + 1].len(),
            last: rows[depth + 1].len(),
        };
        for (i, child) in children.iter().enumerate() {
            let child_type = get_child_type(i, n_children);
            add_nodes_to_rows(
                *child,
                child_type,
                graph,
                nodes,
                rows,
                offsets,
                Some(rows[depth].len()),
            );
        }
        child_pos.last = rows[depth + 1].len() - 1;
        child_cells = Some(child_pos);
    }
    let x = (n.x * 2.) as usize;
    let space_before = x - offsets[depth];
    let width = 2;
    offsets[depth] = x + 2;
    rows[depth].push(TableTreeCell {
        space_before,
        x,
        width,
        node,
        is_first: depth == 0,
        child_type,
        parent_cell,
        child_cells,
    });
}

pub(crate) fn layout(root: usize, graph: &Graph, rows: &mut [TableTreeRow]) {
    let mut nodes = BTreeMap::new();
    DrawTree::add_tree(&mut nodes, root, graph, None, 0, None, 1);
    buchheim(root, graph, &mut nodes);
    let mut offsets = vec![0; rows.len()];
    add_nodes_to_rows(
        root,
        ChildType::Only,
        graph,
        &nodes,
        rows,
        &mut offsets,
        None,
    );
}
