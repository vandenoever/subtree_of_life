use axum::body::Bytes;
use percent_encoding::{utf8_percent_encode, NON_ALPHANUMERIC};
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use reqwest::{Client, ClientBuilder};
use rusqlite::{types::Value, vtab::array::Array, ToSql};
use std::{collections::BTreeMap, error::Error, rc::Rc, sync::Arc};
use subtree_of_life::Sol;
use tokio::sync::Mutex;

pub struct WikiImages {
    client: Client,
    semaphore: Arc<Mutex<()>>,
    image_cache: Pool<SqliteConnectionManager>,
}

impl WikiImages {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        let image_cache = open_cache_db()?;
        Ok(Self {
            client: ClientBuilder::new()
                .user_agent("TaxonBox/0.0 (https://broncode.org; info@broncode.org) reqwest/0.11")
                .https_only(true)
                .build()?,
            semaphore: Arc::new(Mutex::new(())),
            image_cache,
        })
    }
    pub async fn get_image(
        &self,
        sol: &Sol,
        uid: usize,
        image: &str,
    ) -> Result<Bytes, Box<dyn Error>> {
        tracing::debug!("GETTING {} '{}'", uid, image);
        if let Some(bytes) = get_image_from_cache(&self.image_cache, image)? {
            return Ok(bytes);
        }
        // only bother wikicommons with valid image names
        if sol.check_image(uid, image)? {
            let bytes = self.download_image(image).await?;
            if let Err(e) = save_image_to_cache(&self.image_cache, image, &bytes) {
                tracing::error!(e);
            }
            Ok(bytes)
        } else {
            Ok(Default::default())
        }
    }
    pub fn get_image_sizes(
        &self,
        images: &[&str],
    ) -> Result<BTreeMap<String, (usize, usize)>, Box<dyn Error>> {
        get_image_sizes(&self.image_cache, images)
    }
    async fn download_image(&self, image: &str) -> Result<Bytes, Box<dyn Error>> {
        let path = format!(
            "https://commons.wikimedia.org/wiki/Special:FilePath/{}?width=120px",
            utf8_percent_encode(image, NON_ALPHANUMERIC)
        );
        let lock = self.semaphore.lock().await;
        let resp = self.client.get(&path).send().await?;
        let bytes = resp.bytes().await?;
        drop(lock);
        Ok(bytes)
    }
}

fn open_cache_db() -> Result<Pool<SqliteConnectionManager>, Box<dyn Error>> {
    let manager = SqliteConnectionManager::file("db/image_cache.sqlite")
        .with_init(|conn| rusqlite::vtab::array::load_module(conn));
    let image_cache = Pool::<SqliteConnectionManager>::new(manager)?;
    let conn = image_cache.get()?;
    conn.execute_batch(
        "
CREATE TABLE IF NOT EXISTS image (
    name   TEXT NOT NULL PRIMARY KEY,
    width  INTEGER,
    height INTEGER
);
CREATE TABLE IF NOT EXISTS blob (
    name  TEXT NOT NULL PRIMARY KEY,
    blob  BLOB NOT NULL
);
        ",
    )?;
    Ok(image_cache)
}

fn save_image_to_cache(
    pool: &Pool<SqliteConnectionManager>,
    name: &str,
    blob: &Bytes,
) -> Result<(), Box<dyn Error>> {
    let (width, height) = if let Ok(image_size) = imagesize::blob_size(blob) {
        (Some(image_size.width), Some(image_size.height))
    } else {
        (None, None)
    };
    let conn = pool.get()?;
    let mut stmt = conn.prepare("INSERT INTO image(name, width, height) VALUES(?1, ?2, ?3)")?;
    stmt.execute([&name, &width, &height] as [&dyn ToSql; 3])?;
    let mut stmt = conn.prepare("INSERT INTO blob(name, blob) VALUES(?1, ?2)")?;
    let blob: Vec<u8> = blob.to_vec();
    stmt.execute([&name, &blob] as [&dyn ToSql; 2])?;
    Ok(())
}

fn get_image_from_cache(
    pool: &Pool<SqliteConnectionManager>,
    name: &str,
) -> Result<Option<Bytes>, Box<dyn Error>> {
    let conn = pool.get()?;
    let mut stmt = conn.prepare("SELECT blob FROM blob WHERE name = ?1")?;
    let mut rows = stmt.query_map([&name], |row| {
        let blob: Vec<u8> = row.get(0)?;
        Ok(blob)
    })?;
    if let Some(r) = rows.next() {
        return Ok(Some(r?.into()));
    }
    Ok(None)
}

fn get_image_sizes(
    pool: &Pool<SqliteConnectionManager>,
    images: &[&str],
) -> Result<BTreeMap<String, (usize, usize)>, Box<dyn Error>> {
    let images: Array = Rc::new(images.iter().map(|v| Value::Text(v.to_string())).collect());
    let conn = pool.get()?;
    let mut stmt = conn.prepare("SELECT name, width, height FROM image WHERE name IN (SELECT value FROM rarray(?1)) AND width IS NOT NULL AND height IS NOT NULL;")?;
    let rows = stmt.query_map([images], |row| {
        let id = row.get(0)?;
        let width = row.get(1)?;
        let height = row.get(2)?;
        Ok((id, width, height))
    })?;
    let mut sizes = BTreeMap::new();
    for r in rows {
        let r = r?;
        sizes.insert(r.0, (r.1, r.2));
    }
    Ok(sizes)
}
