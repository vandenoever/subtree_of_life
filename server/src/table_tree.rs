use subtree_of_life::Graph;

use crate::tree_layout::layout;

pub type TableTreeRow = Vec<TableTreeCell>;

#[derive(Clone, Copy, PartialEq, Debug)]
pub(crate) enum ChildType {
    Only,
    First,
    Last,
    Middle,
}

#[derive(Debug, Clone, Copy)]
pub struct TableTreeCellChildren {
    pub(crate) first: usize,
    pub(crate) last: usize,
}

#[derive(Debug)]
pub struct TableTreeCell {
    // identifier of the taxon
    pub(crate) node: usize,
    pub space_before: usize,
    pub(crate) x: usize,
    pub width: usize,
    pub(crate) is_first: bool,
    pub(crate) parent_cell: Option<usize>,
    pub(crate) child_cells: Option<TableTreeCellChildren>,
    pub(crate) child_type: ChildType,
}

impl TableTreeCell {
    pub fn node(&self) -> &usize {
        &self.node
    }
    pub fn classes(&self) -> &str {
        if self.child_cells.is_some() {
            "parent_taxon"
        } else {
            "leaf_taxon"
        }
    }
    pub fn child_type(&self) -> &str {
        match self.child_type {
            ChildType::Only => "only_child",
            ChildType::First => "first_child",
            ChildType::Last => "last_child",
            ChildType::Middle => "middle_child",
        }
    }
    pub fn spacer_class(&self) -> &str {
        if self.child_type == ChildType::Last || self.child_type == ChildType::Middle {
            "taxon_cell border_spacer"
        } else {
            ""
        }
    }
    pub fn child_taxon(&self) -> &str {
        if self.is_first {
            ""
        } else {
            "child_taxon"
        }
    }
}

fn max_depth(node: usize, depth: usize, graph: &Graph) -> usize {
    if let Some(children) = graph.get(&node) {
        children
            .iter()
            .map(|c| max_depth(*c, depth + 1, graph))
            .max()
            .unwrap_or(depth + 1)
    } else {
        depth + 1
    }
}

/**
 * There are some rules that the table tree must follow:
 * A parent can not extend beyond its children either to the left or to
 * the right.
 * The left edge of a parent must be above the left edge of the left-most
 * child or to the right of it.
 * Similarly, the right edge of a parent must be above the right-edge of the
 * right-most child or to the left of it.
 * If a parent has only one child, the left and right side of parent and child
 * must align.
 * These rules are required to make sure that the connectors line up.
 */
fn check_table_cell(cell: &TableTreeCell, childrens_row: &TableTreeRow) {
    let (left_child, right_child) = match cell.child_cells {
        None => return,
        Some(child) if child.first == child.last => {
            let child = &childrens_row[child.first];
            if child.x != cell.x || child.width != cell.width {
                eprintln!(
                    "Cell {} should be directly above cell {} and have the same width.",
                    cell.node, child.node
                );
            }
            return;
        }
        Some(child) => (&childrens_row[child.first], &childrens_row[child.last]),
    };
    if cell.x < left_child.x {
        eprintln!(
            "Cell {} extends too far to the left, beyond its left child {}: {} < {}.",
            cell.node, left_child.node, cell.x, left_child.x
        );
    }
    if cell.x + cell.width < left_child.x + left_child.width {
        eprintln!(
            "Cell {} is too thin. It should extend further left.",
            cell.node
        );
    }
    if cell.x + cell.width > right_child.x + right_child.width {
        eprintln!(
            "Cell {} extends too far to the right: {} > {}",
            cell.node,
            cell.x + cell.width,
            right_child.x + right_child.width
        );
    }
    if cell.x > right_child.x {
        eprintln!(
            "Cell {} is too thin. It should extend further right.",
            cell.node,
        );
    }
}

fn check_table_row(row: &TableTreeRow, childrens_row: &TableTreeRow) {
    for cell in row {
        check_table_cell(cell, childrens_row);
    }
}

fn check_table(rows: &[TableTreeRow]) {
    for window in rows.windows(2) {
        check_table_row(&window[0], &window[1]);
    }
}

pub fn create_table_tree(root: Option<usize>, graph: &Graph) -> Vec<TableTreeRow> {
    let mut rows = Vec::new();
    let root = match root {
        None => return rows,
        Some(root) => root,
    };
    let max_depth = max_depth(root, 0, graph);
    rows.resize_with(max_depth, Default::default);
    let buchheim = false;
    if buchheim {
        layout(root, graph, &mut rows);
    } else {
        add_node(0, 0, &mut rows, root, ChildType::Only, graph, None);
        shift_cells_right(&mut rows);
        let mut iter = 0;
        let mut change = true;
        while iter < 10 && change {
            iter += 1;
            change = try_to_center_parents(&mut rows);
        }
    }
    check_table(&rows);
    rows
}

struct NodePosition {
    x: usize,
}

pub(crate) fn get_child_type(i: usize, n_children: usize) -> ChildType {
    if n_children == 1 {
        ChildType::Only
    } else if i == 0 {
        ChildType::First
    } else if i + 1 == n_children {
        ChildType::Last
    } else {
        ChildType::Middle
    }
}

fn get_row_used_width(row: &TableTreeRow) -> usize {
    row.iter().map(|c| c.space_before + c.width).sum()
}

fn add_node(
    depth: usize,
    parent_x: usize,
    rows: &mut [TableTreeRow],
    node: usize,
    child_type: ChildType,
    graph: &Graph,
    parent_cell: Option<usize>,
) -> NodePosition {
    let used_width = get_row_used_width(&rows[depth]);
    // all children except the last one can go as far to the left as they like
    let mut child_x = if child_type == ChildType::Last || child_type == ChildType::Only {
        usize::max(parent_x, used_width)
    } else {
        used_width
    };
    let node_width = 2;
    let mut child_cells = None;
    if let Some(children) = graph.get(&node) {
        let n_children = children.len();
        if n_children > 0 {
            let mut child_pos = TableTreeCellChildren {
                first: rows[depth + 1].len(),
                last: rows[depth + 1].len(),
            };
            for (i, child) in children.iter().enumerate() {
                let child_type = get_child_type(i, n_children);
                let child_pos = add_node(
                    depth + 1,
                    child_x,
                    rows,
                    *child,
                    child_type,
                    graph,
                    Some(rows[depth].len()),
                );
                if i == 0 && child_pos.x > child_x {
                    child_x = child_pos.x;
                }
            }
            child_pos.last = rows[depth + 1].len() - 1;
            child_cells = Some(child_pos);
        }
    }
    let space_before = if child_x > used_width {
        child_x - used_width
    } else {
        0
    };
    rows[depth].push(TableTreeCell {
        space_before,
        x: child_x,
        width: node_width,
        node,
        is_first: depth == 0,
        parent_cell,
        child_cells,
        child_type,
    });
    NodePosition { x: child_x }
}

// After initial layout, cells tend to lie to the left.
// If a cell can shift to the right and keep good connections with the neigh-
// bors to the above and below, it will be moved.
fn shift_cells_right(rows: &mut [TableTreeRow]) {
    let mut to_shift = Vec::new();
    for r in (0..rows.len()).rev() {
        let row = &rows[r];
        let row_len = row.len();
        if row_len > 1 {
            for c in (0..row_len - 1).rev() {
                let cell = &row[c];
                let next_cell = &row[c + 1];
                if next_cell.space_before != 0
                    && cell.parent_cell == next_cell.parent_cell
                    && cell.child_cells.is_none()
                {
                    let parent_x = cell
                        .parent_cell
                        .map(|p| &rows[r - 1][p])
                        .map(|c| c.x)
                        .unwrap_or(0);
                    if parent_x > cell.x {
                        let diff = usize::min(next_cell.space_before, parent_x - cell.x);
                        to_shift.push((r, c, diff));
                    }
                }
            }
        }
    }
    shift_right(to_shift, rows);
}

fn try_to_center_parents(rows: &mut [TableTreeRow]) -> bool {
    let mut to_shift = Vec::new();
    for r in 0..rows.len() - 1 {
        let row = &rows[r];
        let row_len = row.len();
        for c in 0..row_len {
            let cell = &row[c];
            let child_pos = cell.child_cells.map(|p| {
                let row = &rows[r + 1];
                (&row[p.first], &row[p.last])
            });
            if let Some((first, last)) = child_pos {
                let new_x = (first.x + last.x) / 2;
                if new_x > cell.x {
                    let mut diff = new_x - cell.x;
                    if c + 1 < row.len() {
                        let next_cell = &row[c + 1];
                        diff = usize::min(diff, next_cell.space_before);
                    };
                    // TODO: check position of parent of this cell.
                    // This cells x cannot be larger than the parents
                    if let Some(parent) = cell.parent_cell.map(|c| &rows[r - 1][c]) {
                        if parent.x >= cell.x {
                            diff = usize::min(diff, parent.x - cell.x);
                        }
                    }
                    if diff > 0 {
                        to_shift.push((r, c, diff));
                    }
                }
            }
        }
    }
    let change = !to_shift.is_empty();
    shift_right(to_shift, rows);
    change
}

fn shift_right(to_shift: Vec<(usize, usize, usize)>, rows: &mut [TableTreeRow]) {
    for (r, c, diff) in to_shift {
        let row = &mut rows[r];
        let cell = &mut row[c];
        cell.x += diff;
        cell.space_before += diff;
        if c + 1 < row.len() {
            let next_cell = &mut row[c + 1];
            next_cell.space_before -= diff;
        }
    }
}
