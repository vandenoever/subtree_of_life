mod buffer;

use buffer::Buffer;
use std::{error::Error, io::Read};

/// skip initial '['
fn skip_initial_square_bracket(input: &mut impl Read) -> Result<(), Box<dyn Error>> {
    let mut buffer = [0; 1];
    let n = input.read(&mut buffer)?;
    if n != 1 {
        return Err("Could not read initial byte.".into());
    }
    if buffer[0] != b'[' {
        return Err("File does not start with '['.".into());
    }
    Ok(())
}

#[derive(Debug)]
enum Opener {
    SquareBracket,
    CurlyBracket,
    Quote,
}

// stack of open brackets and quotes
type OpenerStack = Vec<Opener>;

fn find_opener(bytes: &[u8]) -> Option<(Result<Opener, Delimiter>, usize)> {
    for (pos, b) in bytes.iter().copied().enumerate() {
        if b == b'[' {
            return Some((Ok(Opener::SquareBracket), pos));
        } else if b == b'{' {
            return Some((Ok(Opener::CurlyBracket), pos));
        } else if b == b'"' {
            return Some((Ok(Opener::Quote), pos));
        } else if b == b',' {
            return Some((Err(Delimiter::Comma), pos));
        } else if b == b']' {
            return Some((Err(Delimiter::End), pos));
        }
    }
    None
}

fn find_opener_or_square_closer(bytes: &[u8]) -> Option<(Option<Opener>, usize)> {
    for (pos, b) in bytes.iter().copied().enumerate() {
        if b == b']' {
            return Some((None, pos));
        } else if b == b'[' {
            return Some((Some(Opener::SquareBracket), pos));
        } else if b == b'{' {
            return Some((Some(Opener::CurlyBracket), pos));
        } else if b == b'"' {
            return Some((Some(Opener::Quote), pos));
        }
    }
    None
}

fn find_opener_or_curly_closer(bytes: &[u8]) -> Option<(Option<Opener>, usize)> {
    for (pos, b) in bytes.iter().copied().enumerate() {
        if b == b'}' {
            return Some((None, pos));
        } else if b == b'[' {
            return Some((Some(Opener::SquareBracket), pos));
        } else if b == b'{' {
            return Some((Some(Opener::CurlyBracket), pos));
        } else if b == b'"' {
            return Some((Some(Opener::Quote), pos));
        }
    }
    None
}

fn find_closing_quote(bytes: &[u8]) -> Option<(Option<Opener>, usize)> {
    bytes
        .iter()
        .enumerate()
        .position(|(pos, b)| *b == b'"' && !is_escaped(bytes, pos))
        .map(|pos| (None, pos))
}

fn find_delimiter(bytes: &[u8]) -> Option<(Delimiter, usize)> {
    for (pos, b) in bytes.iter().copied().enumerate() {
        if b == b',' {
            return Some((Delimiter::Comma, pos));
        } else if b == b']' {
            return Some((Delimiter::End, pos));
        }
    }
    None
}

fn is_escaped(bytes: &[u8], mut pos: usize) -> bool {
    let mut escaped = false;
    while pos > 0 {
        pos -= 1;
        if bytes[pos] != b'\\' {
            return escaped;
        }
        escaped = !escaped;
    }
    escaped
}

#[derive(Debug, PartialEq)]
enum Delimiter {
    Comma,
    End,
}

#[derive(Debug)]
struct ParseResult {
    json_value: Vec<u8>,
    delimiter: Delimiter,
}

fn get_value(
    pos: usize,
    delimiter: Delimiter,
    buffer: &mut Buffer,
    filter: &mut dyn FnMut(&[u8]) -> bool,
) -> ParseResult {
    buffer.consume(pos);
    let json_value = if filter(buffer.current_value()) {
        buffer.consumed_into_vec()
    } else {
        buffer.drop_consumed();
        Vec::new()
    };
    ParseResult {
        json_value,
        delimiter,
    }
}

fn parse_value(
    buffer: &mut Buffer,
    input: &mut impl Read,
    stack: &mut OpenerStack,
    filter: &mut dyn FnMut(&[u8]) -> bool,
) -> Result<ParseResult, Box<dyn Error>> {
    assert!(stack.is_empty());
    loop {
        if let Some((r, pos)) = find_opener(buffer.bytes()) {
            match r {
                Ok(opener) => {
                    buffer.consume(pos + 1);
                    stack.push(opener);
                    break;
                }
                Err(delimiter) => {
                    return Ok(get_value(pos, delimiter, buffer, filter));
                }
            }
        } else {
            buffer.read_more(input)?;
        }
    }
    loop {
        let bytes = buffer.bytes();
        match stack.pop() {
            Some(opener) => {
                let r = match opener {
                    Opener::SquareBracket => find_opener_or_square_closer(bytes),
                    Opener::CurlyBracket => find_opener_or_curly_closer(bytes),
                    Opener::Quote => find_closing_quote(bytes),
                };
                match r {
                    Some((r, p)) => {
                        buffer.consume(p + 1);
                        if let Some(new_opener) = r {
                            stack.push(opener);
                            stack.push(new_opener);
                        }
                    }
                    None => {
                        buffer.read_more(input)?;
                        stack.push(opener);
                    }
                }
            }
            None => {
                if let Some((delimiter, pos)) = find_delimiter(bytes) {
                    return Ok(get_value(pos, delimiter, buffer, filter));
                }
                buffer.read_more(input)?;
            }
        }
    }
}

type FilterFn = dyn FnMut(&[u8]) -> bool;

pub struct JsonSplitter<R> {
    input: R,
    buffer: Buffer,
    stack: OpenerStack,
    done: bool,
    filter: Box<FilterFn>,
}

impl<R: Read> JsonSplitter<R> {
    pub fn from_read(mut input: R) -> Result<Self, Box<dyn Error>> {
        skip_initial_square_bracket(&mut input)?;
        Ok(Self {
            input,
            buffer: Buffer::new(),
            stack: OpenerStack::default(),
            done: false,
            filter: Box::new(|_| true),
        })
    }
    pub fn set_filter(&mut self, filter: Box<FilterFn>) {
        self.filter = filter;
    }
    pub fn position(&self) -> u64 {
        self.buffer.total_read()
    }
}

impl<R: Read> Iterator for JsonSplitter<R> {
    type Item = Result<Vec<u8>, Box<dyn Error>>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }
        loop {
            let r = parse_value(
                &mut self.buffer,
                &mut self.input,
                &mut self.stack,
                &mut self.filter,
            );
            let r = match r {
                Err(e) => {
                    self.done = true;
                    return Some(Err(e));
                }
                Ok(r) => r,
            };
            if r.delimiter == Delimiter::End {
                assert!(self.stack.is_empty());
                self.done = true;
                if r.json_value.is_empty() {
                    return None;
                }
            }
            if !r.json_value.is_empty() {
                return Some(Ok(r.json_value));
            }
        }
    }
}

pub fn split_json_array_stream(
    input: &mut impl Read,
    handler: &mut dyn FnMut(Vec<u8>),
) -> Result<(), Box<dyn Error>> {
    let splitter = JsonSplitter::from_read(input)?;
    for v in splitter {
        handler(v?);
    }
    Ok(())
}

#[cfg(test)]
fn test_ok(data: &[u8]) -> Vec<String> {
    let mut cursor = std::io::Cursor::new(data);
    let mut values = Vec::new();
    let splitter = JsonSplitter::from_read(&mut cursor).unwrap();
    for v in splitter {
        values.push(String::from_utf8(v.unwrap()).unwrap());
    }
    if cursor.position() as usize != data.len() {
        panic!(
            "not the entire stream was parsed, only {} of {}",
            cursor.position(),
            data.len()
        );
    }
    values
}

#[test]
fn empty() {
    let r = test_ok(b"[]");
    assert!(r.is_empty());
}
#[test]
fn trailing_ws() {
    let r = test_ok(b"[] \n \r");
    assert!(r.is_empty());
}
#[test]
fn len_one() {
    let r = test_ok(b"[1]");
    assert_eq!(r.len(), 1);
}
#[test]
fn len_one_array() {
    let r = test_ok(b"[[]]");
    assert_eq!(r.len(), 1);
}
#[test]
fn len_one_object() {
    let r = test_ok(b"[{}]");
    assert_eq!(r.len(), 1);
}
#[test]
fn len_two() {
    let r = test_ok(b"[1,2]");
    assert_eq!(r.len(), 2);
}
#[test]
fn len_two_array() {
    let r = test_ok(b"[[],[]]");
    assert_eq!(r.len(), 2);
}
#[test]
fn len_two_object() {
    let r = test_ok(b"[{},{\"a\":{}}]");
    assert_eq!(r.len(), 2);
}
#[test]
fn len_two_number_object() {
    let r = test_ok(b"[1,{\"a\":{}}]");
    assert_eq!(r.len(), 2);
}
#[test]
fn complex_object() {
    let r = test_ok(b"[1,{\"a\":{},\"P10565\":[{\"mainsnak\":{\"datatype\":\"external-id\",\"datavalue\":{\"type\":\"string\",\"value\":171981},\"property\":\"P10565\"}}]}]");
    assert_eq!(r.len(), 2);
}
#[test]
fn complex_object2() {
    let r = test_ok(b"[1,{\"a\":{},\"P10565\":[{\"mainsnak\":{\"datatype\":\"external-id\",\"datavalue\":{\"type\":3,\"value\":171981},\"property\":\"P10565\"}}]}]");
    assert_eq!(r.len(), 2);
}
#[test]
fn complex_object3() {
    let r = test_ok(b"[1,[{\"\":{\"property\":\"P10565\"}}]]");
    assert_eq!(r.len(), 2);
}
#[test]
fn escape() {
    let r = test_ok(b"[{\"\\\"\":0}]");
    assert_eq!(r.len(), 1);
}
#[test]
fn double_escape() {
    let r = test_ok(b"[{\"\\\\\\\"\":0}]");
    assert_eq!(r.len(), 1);
}
