use std::{collections::HashMap, error::Error, path::Path};

use rusqlite::{Connection, ToSql};

use crate::{synonyms::Synonyms, Graph};

fn read_synonyms(conn: &Connection) -> Result<Synonyms, Box<dyn Error>> {
    let mut synonyms = Synonyms::default();
    let mut stmt = conn.prepare("SELECT json.id, json_extract(o.value, '$.mainsnak.datavalue.value.numeric-id') as synonym from json, json_each(json.json, '$.claims.P1420') as o where synonym is not null order by json.id, synonym;")?;
    let rows = stmt.query_map(&[] as &[&dyn ToSql], |row| {
        let id: usize = row.get(0)?;
        let synonym: usize = row.get(1)?;
        Ok((id, synonym))
    })?;
    for value in rows {
        let (taxon, synonym) = value?;
        synonyms.add_synonym(taxon, synonym);
    }
    // 1378800 Euteleostomi, 27207 Osteichthyes
    synonyms.add_synonym(1378800, 27207);
    Ok(synonyms)
}

fn read_taxon_parents(conn: &Connection) -> Result<Graph, Box<dyn Error>> {
    let mut graph = Graph::new();
    let mut stmt = conn.prepare("SELECT json.id, json_extract(parent.value, '$.mainsnak.datavalue.value.numeric-id') as p from json, json_each(json.json, '$.claims.P171') as parent where p is not null")?;
    let rows = stmt.query_map(&[] as &[&dyn ToSql], |row| {
        let id: usize = row.get(0)?;
        let parent: usize = row.get(1)?;
        Ok((id, parent))
    })?;
    for value in rows {
        let (id, parent) = value?;
        graph.entry(parent).or_default().insert(id);
    }
    Ok(graph)
}

pub fn replace_synonyms(graph: &mut Graph, synonyms: &Synonyms) {
    let keys: Vec<usize> = graph.keys().cloned().collect();
    for k in keys {
        if let Some(main) = synonyms.get_main_synonym(k) {
            if let Some(v) = graph.remove(&k) {
                let main = graph.entry(main).or_default();
                for v in v {
                    main.insert(v);
                }
            }
        }
    }
    let mut replacements = HashMap::new();
    for children in graph.values_mut() {
        replacements.clear();
        for v in children.iter() {
            if let Some(main) = synonyms.get_main_synonym(*v) {
                replacements.insert(*v, main);
            }
        }
        for (k, v) in &replacements {
            children.remove(k);
            children.insert(*v);
        }
    }
}

fn write_taxon_parents(conn: &Connection, graph: &Graph) -> Result<(), Box<dyn Error>> {
    println!("before write_taxon_parents");
    conn.execute_batch(
        "
DELETE FROM taxon;
        ",
    )?;
    conn.cache_flush()?;
    println!("before w write_taxon_parents");
    let mut stmt = conn.prepare("INSERT INTO taxon(id, parent) VALUES(?1, ?2)")?;
    println!("before x write_taxon_parents");
    for (parent, taxons) in graph {
        for taxon in taxons {
            stmt.execute([&taxon as &dyn ToSql, &parent])?;
        }
    }
    Ok(())
}

pub fn postprocess_db(db_path: &Path) -> Result<(), Box<dyn Error>> {
    let mut graph;
    {
        let conn = super::open_db(db_path, false)?;
        let synonyms = read_synonyms(&conn)?;
        println!("read {} synonyms", synonyms.len());
        graph = read_taxon_parents(&conn)?;
        println!("read {} parent", graph.len());
        replace_synonyms(&mut graph, &synonyms);
        println!("{} nodes after replacing synonyms", graph.len());
        super::clean_graph(&mut graph);
        println!("{} nodes after cleaning", graph.len());
    }
    let conn = super::open_db(db_path, true)?;
    write_taxon_parents(&conn, &graph)?;
    Ok(())
}
