use super::Result;
use crate::{Graph, TaxonInfo};
use std::{
    collections::HashMap,
    fmt::Write,
    process::{Command, Stdio},
};

pub fn simple_graph(
    graph: &Graph,
    info: &HashMap<usize, TaxonInfo>,
    uids: &[usize],
) -> Result<(String, String)> {
    let dot = draw_graph(uids, info, graph)?;
    let svg = dot_to_svg(dot.clone())?;
    Ok((dot, svg))
}

fn get_parent(graph: &Graph, mut uid: usize) -> Option<usize> {
    while let Some((k, v)) = graph.iter().find(|(_, v)| v.contains(&uid)) {
        if v.len() > 1 {
            return Some(*k);
        }
        uid = *k;
    }
    None
}

fn define_node(
    dot: &mut String,
    uid: usize,
    entries: &HashMap<usize, TaxonInfo>,
    graph: &Graph,
) -> std::fmt::Result {
    if let Some(entry) = entries.get(&uid) {
        writeln!(
        dot,
        "n{}[label=\"{}\",class=\"{}\",href=\"https://tree.opentreeoflife.org/taxonomy/browse?id={}\",tooltip=\"{}\"];",
        uid, entry.name, entry.rank,uid, entry.rank
    )?;
    }
    if let Some(parent) = get_parent(graph, uid) {
        writeln!(dot, "n{} -> n{}", parent, uid)?;
    }
    Ok(())
}

fn draw_graph(
    uids: &[usize],
    entries: &HashMap<usize, TaxonInfo>,
    graph: &Graph,
) -> Result<String> {
    let mut dot = String::new();
    writeln!(dot, "digraph {{")?;
    for (uid, children) in graph {
        if !children.is_empty() {
            define_node(&mut dot, *uid, entries, graph)?;
        }
    }
    for uid in uids {
        define_node(&mut dot, *uid, entries, graph)?;
    }
    writeln!(dot, "}}")?;
    Ok(dot)
}

fn dot_to_svg(dot: String) -> Result<String> {
    let mut child = Command::new("dot")
        .args(["-Tsvg"])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;
    let mut stdin = child.stdin.take().expect("Failed to open stdin");
    std::thread::spawn(move || {
        use std::io::Write;
        stdin
            .write_all(dot.as_bytes())
            .expect("Failed to write to stdin");
    });
    let output = child.wait_with_output()?;
    Ok(String::from_utf8_lossy(&output.stdout).to_string())
}
