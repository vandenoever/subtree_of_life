use std::collections::{HashMap, HashSet};

#[derive(Default)]
pub struct Synonyms {
    map: HashMap<usize, HashSet<usize>>,
    synonyms: HashMap<usize, usize>,
}

impl Synonyms {
    pub fn len(&self) -> usize {
        self.map.len()
    }
    pub fn is_empty(&self) -> bool {
        self.map.is_empty()
    }
    pub fn get_main_synonym(&self, v: usize) -> Option<usize> {
        self.synonyms.get(&v).cloned()
    }
    pub fn add_synonym(&mut self, mut taxon: usize, mut synonym: usize) {
        if taxon > synonym {
            std::mem::swap(&mut synonym, &mut taxon);
        }
        if let Some(m) = self.get_main_synonym(taxon) {
            // taxon is already a synonym, just add (or move) synonym
            self.add_to_main(m, synonym);
        } else if let Some(m) = self.get_main_synonym(synonym) {
            self.add_to_main(m, taxon);
        } else {
            self.synonyms.insert(synonym, taxon);
            self.map.entry(taxon).or_default().insert(synonym);
        }
    }
    fn add_to_main(&mut self, m: usize, synonym: usize) {
        let old_synonym = self.map.remove(&synonym);
        let entry = self.map.entry(m).or_default();
        if let Some(old_synonym) = old_synonym {
            for o in old_synonym {
                entry.insert(o);
                self.synonyms.insert(o, m);
            }
        }
        entry.insert(synonym);
        self.synonyms.insert(synonym, m);
    }
}
