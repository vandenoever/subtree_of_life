use regex::bytes::Regex;
use serde_json::Value;
use std::{error::Error, time::Instant};
use subtree_of_life::json_splitter::JsonSplitter;

fn main() -> Result<(), Box<dyn Error>> {
    // std::io::stdin is buffered, opening stdin directly is ~10% faster
    let mut input = std::fs::File::open("/dev/stdin")?;
    let mut n = 0;
    let mut f = 0;
    let mut json_splitter = JsonSplitter::from_read(&mut input)?;

    let now = Instant::now();
    let re = Regex::new(r"P10585").unwrap();
    json_splitter.set_filter(Box::new(move |v| {
        f += 1;
        if f % 1000000 == 0 {
            let rate = f / now.elapsed().as_millis();
            println!("filtered {} values. {}/ms", f, rate);
        }
        re.is_match(v)
    }));
    while let Some(v) = json_splitter.next() {
        let v = v?;
        if let Err(e) = serde_json::from_slice::<Value>(&v) {
            eprintln!("Could not parse '{}': {}", String::from_utf8(v).unwrap(), e);
        }
        n += 1;
        if n % 100000 == 0 {
            let rate =
                json_splitter.position() as u128 * 1000 / now.elapsed().as_millis() / (1024 * 1024);
            println!(
                "read {} values from {} GB ({} MB/s)",
                n,
                json_splitter.position() / (1024 * 1024 * 1024),
                rate
            );
        }
    }
    eprintln!("read {} values", n);
    Ok(())
}
