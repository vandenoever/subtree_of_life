use std::{env::args, error::Error, path::PathBuf};
use subtree_of_life::{iter_json_from_stdin, parse_ott_from_stdin};

fn main() -> Result<(), Box<dyn Error>> {
    let db_path = PathBuf::from("db/life.sqlite");
    let json_db_path = PathBuf::from("db/wikidata_json.sqlite");
    let mut args = args();
    let _exe = args.next().unwrap();
    let mode = args.next().expect("No mode provided on the command-line.");
    // load ott taxonomy.tsv into an sqlite db
    // < ott3.3/taxonomy.tsv target/release/sol parse_ott
    if mode == "parse_ott" {
        parse_ott_from_stdin(&db_path)?;
        return Ok(());
    }
    // load latest-all.json.zstd into an sqlite db
    if mode == "parse" {
        iter_json_from_stdin(&json_db_path)?;
        return Ok(());
    }
    Err(format!("Unknown mode {}", mode).into())
}
