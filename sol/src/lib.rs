pub mod dot;
pub mod json_splitter;
pub mod postprocess;
pub mod synonyms;

use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use regex::bytes::RegexSet;
use rusqlite::{types, vtab::array::Array, Connection, OpenFlags, Statement, ToSql};
use serde_json::Value;
use std::{
    collections::{BTreeMap, BTreeSet, HashMap, HashSet},
    error::Error,
    fmt::{Display, Formatter},
    path::Path,
    rc::Rc,
    str::FromStr,
    sync::mpsc::{channel, sync_channel, Receiver, Sender},
    thread::JoinHandle,
};

type Result<T> = std::result::Result<T, Box<dyn Error>>;

fn take_out_branchless_nodes(graph: &mut Graph, uids: &[usize]) {
    let mut to_remove = Vec::with_capacity(graph.len());
    for (key, value) in graph.iter() {
        if !uids.contains(key) && value.len() == 1 {
            to_remove.push(*key);
        }
    }
    for key in to_remove {
        let child = *graph[&key].iter().next().unwrap();
        if let Some(parent) = graph
            .iter()
            .find(|(_, v)| v.contains(&key))
            .map(|(k, _)| *k)
        {
            if let Some(entry) = graph.get_mut(&parent) {
                entry.remove(&key);
                entry.insert(child);
            }
        }
        graph.remove(&key);
    }
}

pub fn simple_graph_data(
    sol: &Sol,
    uids: &[usize],
    languages: &[&str],
) -> Result<(Graph, HashMap<usize, TaxonInfo>)> {
    let conn = sol.pool.get()?;
    let mut graph = load_db_graph(&conn, uids)?;
    take_out_branchless_nodes(&mut graph, uids);
    // if the graph is empty, add the requested uids as nodes
    if graph.is_empty() {
        for uid in uids {
            graph.entry(*uid).or_default();
        }
    }
    let mut nodes: Vec<usize> = graph.keys().copied().collect();
    for uid in uids {
        nodes.push(*uid);
    }
    let info = load_info(sol, nodes.iter(), languages)?;
    Ok((graph, info))
}

fn load_db_graph(conn: &Connection, uids: &[usize]) -> Result<Graph> {
    let mut graph = Default::default();
    if uids.is_empty() {
        return Ok(graph);
    }
    let uids = uids_to_array(uids.iter());
    let stmt = "
        WITH RECURSIVE
          ancestor(id, parent) AS (
            SELECT id, parent
              FROM ott_taxon WHERE id IN (SELECT value FROM rarray(?1))
            UNION ALL
            SELECT t.id, t.parent FROM ott_taxon t
              JOIN ancestor a ON a.parent = t.id
          )
        SELECT DISTINCT id, parent FROM ancestor WHERE parent IS NOT NULL
        ";
    let mut stmt = conn.prepare(stmt).unwrap();
    let rows = stmt.query_map([&uids], |row| Ok((row.get(0)?, row.get(1)?)))?;
    for value in rows {
        let (id, parent) = value?;
        graph.entry(parent).or_default().insert(id);
    }
    Ok(graph)
}

pub fn open_db(db_path: &Path, writable: bool) -> Result<Connection> {
    let mut flags = OpenFlags::SQLITE_OPEN_NO_MUTEX;
    if writable {
        flags |= OpenFlags::SQLITE_OPEN_READ_WRITE | OpenFlags::SQLITE_OPEN_CREATE;
    } else {
        flags |= OpenFlags::SQLITE_OPEN_READ_ONLY
    }
    let conn = Connection::open(db_path)?;
    rusqlite::vtab::array::load_module(&conn)?;
    conn.execute_batch(
        "
         PRAGMA journal_mode = OFF;
         PRAGMA synchronous = OFF;
         PRAGMA temp_store = MEMORY;
         PRAGMA cache_size = -64000;
         PRAGMA mmap_size = 300000000000;
         PRAGMA foreign_keys = ON;
         PRAGMA defer_foreign_keys = ON;
            ",
    )?;
    Ok(conn)
}

fn start_db_writing(
    db_path: &Path,
    filter: WikiDataFilter,
) -> Result<(JoinHandle<()>, Sender<Vec<u8>>)> {
    let conn = open_db(db_path, true)?;
    // the average size of an extracted wikidata json object is 5600 bytes
    conn.execute_batch(
        "
BEGIN;
CREATE TABLE json (
    id    INTEGER PRIMARY KEY,
    json  TEXT NOT NULL
);
COMMIT;
        ",
    )?;
    let (rx, tx) = channel::<Vec<u8>>();
    let handle = std::thread::spawn(move || {
        let mut stmt = conn
            .prepare("INSERT INTO json(id, json) VALUES(?1, ?2)")
            .unwrap();
        let mut n = 0;
        while let Ok(value) = tx.recv() {
            if let Ok(mut value) = serde_json::from_slice::<Value>(&value) {
                if let Some(id) = get_id(&value) {
                    if keep_value(id, &mut value, &filter) {
                        stmt.execute([&id as &dyn ToSql, &value]).unwrap();
                        n += 1;
                        if n % 10000 == 0 {
                            println!("Found it! {}", n);
                        }
                    }
                }
            }
        }
        conn.execute_batch("
CREATE TABLE claim AS SELECT json.id, CAST(substr(c.key,2) AS INTEGER) AS claim, json_extract(v.value, '$.mainsnak.datavalue.value') AS value FROM json, json_each(json.json, '$.claims') AS c, json_each(c.value, '$') AS v ORDER BY json.id, claim, value;
CREATE INDEX claim_id ON claim(id);
CREATE INDEX claim_claim ON claim(claim);
CREATE TABLE label AS SELECT json.id, l.key AS language, json_extract(l.value, '$.value') AS label FROM json, json_each(json.json, '$.labels') AS l ORDER BY json.id, language, label;
CREATE INDEX label_id_language ON label(id,language);
CREATE INDEX json.label_label ON label(label COLLATE NOCASE);
CREATE TABLE alias AS SELECT json.id, a.key AS language, json_extract(b.value, '$.value') AS alias
FROM
  json,
  json_each(json.json, '$.aliases') AS a,
  json_each(a.value) AS b
ORDER BY json.id, language, alias;
CREATE INDEX alias_id ON alias(id);
CREATE INDEX alias_alias ON alias(alias COLLATE NOCASE);
CREATE TABLE sitelink AS SELECT json.id, substr(a.key, 1, length(a.key)-4) AS wiki, json_extract(a.value, '$.title') AS link
FROM
  json,
  json_each(json.json, '$.sitelinks') AS a
WHERE a.key LIKE '%wiki' ORDER BY json.id,wiki,link;
ANALYZE;").unwrap();
    });
    Ok((handle, rx))
}

fn get_id(value: &Value) -> Option<u64> {
    if let Value::Object(object) = &value {
        if let Some(Value::String(id)) = object.get("id") {
            if let Some(id) = id.strip_prefix('Q') {
                return u64::from_str(id).ok();
            }
        }
    }
    None
}

#[allow(unused)]
fn start_db_reading(db_path: &Path) -> Result<Receiver<Value>> {
    let conn = open_db(db_path, false)?;
    let (tx, rx) = sync_channel(100);
    std::thread::spawn(move || {
        let mut stmt = conn.prepare("SELECT json FROM json").unwrap();
        let rows = stmt
            .query_map(&[] as &[&dyn ToSql], |row| {
                let value: Value = row.get::<_, Value>(0).unwrap();
                Ok(value)
            })
            .unwrap();
        for value in rows {
            tx.send(value.unwrap()).unwrap();
        }
    });
    Ok(rx)
}

fn numeric_id(instance_of: &Value) -> Option<u64> {
    if let Value::Array(array) = instance_of {
        for item in array {
            if let Value::Object(object) = item {
                if let Some(Value::Object(mainsnak)) = object.get("mainsnak") {
                    if let Some(Value::Object(datavalue)) = mainsnak.get("datavalue") {
                        if let Some(Value::Object(value)) = datavalue.get("value") {
                            if let Some(Value::Number(numeric_id)) = value.get("numeric-id") {
                                if let Some(numeric_id) = numeric_id.as_u64() {
                                    return Some(numeric_id);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    None
}

#[derive(Clone)]
struct WikiDataFilter {
    // Instance have the form Q123, 'Q' followed by a positive integer.
    // `instances` only stores the integer.
    instances: Vec<u64>,
    // Claims have the form P123, 'P' followed by a positive integer.
    // `claims` only stores the integer.
    claims: Vec<usize>,
}

/// Create RegexSet that matches instances and claim keys in a serialized
/// JSON object.
fn create_regex_for_json_string(filter: &WikiDataFilter) -> RegexSet {
    let mut expressions = Vec::with_capacity(filter.instances.len() | filter.claims.len());
    for instance in &filter.instances {
        expressions.push(regex::escape(&format!("\"id\":\"Q{}\"", instance)));
    }
    for claim in &filter.claims {
        expressions.push(regex::escape(&format!("\"P{}\":", claim)));
    }
    RegexSet::new(&expressions).unwrap()
}

fn add_additional_instances(filter: &mut WikiDataFilter) {
    let mut vec = Vec::new();
    let csv = std::fs::read_to_string("manual_links.csv").unwrap();
    for line in csv.lines() {
        let mut fields = line.split(',');
        let wikidata_id = u64::from_str(fields.next().unwrap()).unwrap();
        vec.push(wikidata_id);
    }
    filter.instances.extend(vec);
}

fn wiki_data_filter() -> WikiDataFilter {
    WikiDataFilter {
        // Q16521 is taxon
        // Q427626 is taxonomic rank
        // Q713623 is a clade, subproperty of Q16521
        // Q310890 is monotypic taxon
        instances: vec![3, 16521, 427626, 713623, 310890],
        // parent taxon P171
        // taxon name  P225
        // taxon rank  P105
        // GBIF taxon ID P846
        // Catalogue of Life ID P10585
        // IRMNG ID P5055
        // Encyclopedia of Life ID P830
        // World Flora Online ID P7715
        // IPNI plant ID P961
        // iNaturalist taxon ID P3151
        // taxon common name P1843
        // WoRMS-ID for taxa P850
        // ITIS TSN P815
        // Plants of the World online ID P5037
        // Tropicos ID P960
        // NCBI taxonomy ID P685
        // Open Tree of Life ID P9157
        // PlantList-ID
        // taxonomic type P427
        // Tree of Life Web Project ID P5221
        claims: vec![
            171, 225, 105, 846, 10585, 5055, 830, 7715, 961, 3151, 1843, 850, 815, 5037, 960, 685,
            9157, 1070, 427, 5221,
        ],
    }
}

#[test]
fn test_positive_raw_wiki_data_match() {
    let filter = wiki_data_filter();
    let regex = create_regex_for_json_string(&filter);
    assert!(regex.is_match(b"\"P171\":"));
}

#[test]
fn test_negative_raw_wiki_data_match() {
    let filter = wiki_data_filter();
    let regex = create_regex_for_json_string(&filter);
    assert!(!regex.is_match(b"P171\":"));
}

fn keep_value(id: u64, value: &mut Value, filter: &WikiDataFilter) -> bool {
    let mut keep_value = filter.instances.contains(&id);
    if let Value::Object(map) = value {
        if let Some(Value::Object(claims)) = map.get_mut("claims") {
            // if value is a taxon or a taxonomic rank
            if let Some(instance_of) = claims.get("P31") {
                if let Some(numeric_id) = numeric_id(instance_of) {
                    keep_value |= filter.instances.contains(&numeric_id);
                }
            }
            keep_value |= claims.keys().any(|k| {
                if let Some(k) = k.strip_prefix('P').and_then(|p| usize::from_str(p).ok()) {
                    filter.claims.contains(&k)
                } else {
                    false
                }
            });
            if keep_value {
                for v in claims.values_mut() {
                    if let Value::Array(items) = v {
                        for item in items {
                            if let Value::Object(v) = item {
                                v.remove("references");
                            }
                        }
                    }
                }
            }
        }
    }
    keep_value
}

pub fn iter_json_from_stdin(db_path: &Path) -> Result<()> {
    if db_path.exists() {
        return Err(format!("The file {} already exists.", db_path.display()).into());
    }
    let mut filter = wiki_data_filter();
    add_additional_instances(&mut filter);
    let (thread, sender) = start_db_writing(db_path, filter.clone())?;
    let stdin = std::io::stdin();
    let handle = stdin.lock();
    let mut iter = json_splitter::JsonSplitter::from_read(handle)?;
    let regex = create_regex_for_json_string(&filter);
    iter.set_filter(Box::new(move |bytes| regex.is_match(bytes)));
    for i in iter.filter_map(|v| v.ok()) {
        sender.send(i)?;
    }
    drop(sender);
    thread.join().unwrap();
    Ok(())
}

pub fn parse_ott_from_stdin(db_path: &Path) -> Result<()> {
    let stdin = std::io::stdin();
    let conn = open_db(db_path, true)?;
    conn.execute_batch(
        "
CREATE TABLE ott_taxon (
    id    INTEGER PRIMARY KEY,
    parent INTEGER
);
CREATE TABLE ott_label (
    id    INTEGER PRIMARY KEY,
    label TEXT NOT NULL,
    rank  TEXT NOT NULL
);
CREATE TABLE ott_gbif (
    ott   INTEGER NOT NULL,
    gbif  INTEGER NOT NULL,
    UNIQUE(ott, gbif)
);
CREATE TABLE ott_irmng (
    ott   INTEGER NOT NULL,
    irmng INTEGER NOT NULL,
    UNIQUE(ott, irmng)
);
        ",
    )?;
    let mut buffer = String::new();
    let mut lines = 0;
    let mut uid;
    let mut taxon_stmt = conn.prepare("INSERT INTO ott_taxon(id, parent) VALUES(?1, ?2)")?;
    let mut label_stmt =
        conn.prepare("INSERT INTO ott_label(id, label, rank) VALUES(?1, ?2, ?3)")?;
    let mut gbif_stmt = conn.prepare("INSERT INTO ott_gbif(ott, gbif) VALUES(?1, ?2)")?;
    let mut irmng_stmt = conn.prepare("INSERT INTO ott_irmng(ott, irmng) VALUES(?1, ?2)")?;
    while let Ok(n) = stdin.read_line(&mut buffer) {
        if n == 0 {
            break;
        }
        lines += 1;
        // skip header
        if lines == 1 {
            buffer.clear();
            continue;
        }
        let mut fields = buffer.split("\t|\t");
        uid = usize::from_str(fields.next().unwrap())?;
        let parent = fields.next().unwrap();
        let parent = if parent.is_empty() {
            None
        } else {
            Some(usize::from_str(parent)?)
        };
        taxon_stmt.execute([&uid as &dyn ToSql, &parent])?;
        let name = fields.next().unwrap();
        let rank = fields.next().unwrap();
        if let Some(refs) = fields.next() {
            get_refs(uid, refs, &mut gbif_stmt, &mut irmng_stmt)?;
        }
        label_stmt.execute([&uid as &dyn ToSql, &name, &rank])?;
        buffer.clear();
    }
    conn.execute_batch("CREATE INDEX ott_label_label ON ott_label(label COLLATE NOCASE);")?;
    Ok(())
}

fn get_refs(
    uid: usize,
    refs: &str,
    gbif_stmt: &mut Statement,
    irmng_stmt: &mut Statement,
) -> Result<()> {
    for r in refs.split(',') {
        if let Some(g) = r.strip_prefix("gbif:") {
            let gbif = usize::from_str(g)?;
            gbif_stmt.execute([&uid, &gbif])?;
        } else if let Some(g) = r.strip_prefix("irmng:") {
            if let Ok(irmng) = usize::from_str(g) {
                irmng_stmt.execute([&uid, &irmng])?;
            } else {
                eprintln!("Cannot parse integer '{}'", g);
            }
        }
    }
    Ok(())
}

pub type Graph = BTreeMap<usize, BTreeSet<usize>>;

fn get_top_nodes(graph: &Graph) -> HashSet<usize> {
    let children: HashSet<usize> = graph.values().fold(HashSet::new(), |mut s, v| {
        for c in v {
            s.insert(*c);
        }
        s
    });
    graph
        .keys()
        .filter(|k| !children.contains(k))
        .cloned()
        .collect()
}

fn has_child(
    graph: &Graph,
    node: usize,
    possible_child: usize,
    parents: &mut HashSet<usize>,
    memo: &mut HashMap<(usize, usize), bool>,
) -> bool {
    if parents.contains(&node) {
        return false;
    }
    if let Some(r) = memo.get(&(node, possible_child)) {
        return *r;
    }
    parents.insert(node);
    let r = if let Some(v) = graph.get(&node) {
        if v.contains(&possible_child) {
            true
        } else {
            v.iter()
                .any(|c| has_child(graph, *c, possible_child, parents, memo))
        }
    } else {
        false
    };
    memo.insert((node, possible_child), r);
    if memo.len() % 10000000 == 0 {
        println!("memo increased to {}", memo.len());
    }
    parents.remove(&node);
    r
}

fn break_graph_circles(
    graph: &mut Graph,
    node: usize,
    parents: &mut HashSet<usize>,
    mut count: usize,
) -> usize {
    parents.insert(node);
    if let Some(v) = graph.get_mut(&node) {
        v.retain(|v| !parents.contains(v));
        let v = v.clone();
        for c in v.iter() {
            count = break_graph_circles(graph, *c, parents, count + 1);
        }
    }
    parents.remove(&node);
    if count % 10000000 == 0 {
        println!(
            "break_graph_circles for {}: {} {}",
            node,
            count,
            parents.len()
        );
    }
    count
}

// remove direct children of the node that also occur deeper in the tree
fn clean_graph_node(
    graph: &mut Graph,
    node: usize,
    parents: &mut HashSet<usize>,
    memo: &mut HashMap<(usize, usize), bool>,
) {
    if parents.contains(&node) {
        return;
    }
    parents.insert(node);
    let mut left = BTreeSet::new();
    if let Some(v) = graph.get(&node) {
        left = v.clone();
        let children: Vec<usize> = left.iter().cloned().collect();
        for n in children.iter() {
            for c in children.iter() {
                if c != n && left.contains(c) && has_child(graph, *n, *c, parents, memo) {
                    left.remove(c);
                }
            }
        }
    }
    for c in left.iter() {
        clean_graph_node(graph, *c, parents, memo);
    }
    if let Some(v) = graph.get_mut(&node) {
        *v = left;
    }
    parents.remove(&node);
}

fn clean_graph(graph: &mut Graph) {
    let top_nodes = get_top_nodes(graph);
    println!("Found {} top nodes.", top_nodes.len());
    let mut parents = HashSet::new();
    let mut memo = HashMap::new();
    for (n, top_node) in top_nodes.iter().enumerate() {
        println!("{}: cleaning {}", n, top_node);
        assert!(parents.is_empty());
        clean_graph_node(graph, *top_node, &mut parents, &mut memo);
    }
    println!("Done cleaning nodes.");
    for (n, top_node) in top_nodes.iter().enumerate() {
        println!("{}: breaking cycles for {}", n, top_node);
        assert!(parents.is_empty());
        break_graph_circles(graph, *top_node, &mut parents, 0);
    }
    println!("Done breaking graph cycles.");
}

#[derive(Debug, Clone)]
pub struct TaxonImage {
    pub name: String,
}

#[derive(Debug, Clone, Default)]
pub struct Rank {
    pub name: String,
}

impl Rank {
    pub fn is_genus_or_lower(&self) -> bool {
        ["species", "subspecies", "genus", "varietas"].contains(&self.name.as_str())
    }
    pub fn as_str(&self) -> &str {
        self.name.as_str()
    }
    pub fn split_varietas<'a>(&self, name: &'a str) -> (&'a str, &'a str) {
        if let Some(pos) = name.find(" var. ") {
            return (&name[..pos], &name[pos + 6..]);
        }
        (name, "")
    }
}

impl Display for Rank {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

#[derive(Debug, Clone)]
pub struct WikiLink {
    pub lang: String,
    pub name: String,
}

#[derive(Debug, Clone, Default)]
pub struct TaxonInfo {
    pub uid: usize,
    pub name: String,
    pub label: String,
    pub alias: Vec<String>,
    pub common_name: Vec<String>,
    pub wiki_link: Option<WikiLink>,
    pub gbif_id: Option<usize>,
    pub rank: Rank,
    pub images: Vec<TaxonImage>,
    pub year: Option<i64>,
}

pub fn uids_to_array<'a>(uids: impl Iterator<Item = &'a usize>) -> Array {
    Rc::new(uids.map(|v| (*v as i64).into()).collect())
}

pub fn load_info<'a>(
    sol: &Sol,
    uids: impl Iterator<Item = &'a usize>,
    languages: &[&str],
) -> Result<HashMap<usize, TaxonInfo>> {
    let conn = sol.pool.get()?;
    let mut results: HashMap<usize, TaxonInfo> = HashMap::new();
    let uids = uids_to_array(uids);
    let mut langs: Vec<types::Value> = Vec::with_capacity(languages.len() + 1);
    langs.push(types::Value::Null);
    langs.extend(languages.iter().map(|v| v.to_string().into()));
    let langs: Array = Rc::new(langs);
    let mut stmt = conn.prepare(
        "
SELECT id, language, label, alias, common_name, wiki_link, default_wiki_link
  FROM name
  JOIN (SELECT rowid, value FROM rarray(?1)) l ON language IS l.value
 WHERE id IN (SELECT value FROM rarray(?2)) ORDER BY id, l.rowid",
    )?;
    let rows = stmt.query_map([&langs as &dyn ToSql, &uids], |row| {
        let uid: usize = row.get(0)?;
        let lang: Option<String> = row.get(1)?;
        let label: Option<String> = row.get(2)?;
        let default_wiki_link: Option<bool> = row.get(6)?;
        let default_wiki_link = default_wiki_link.unwrap_or_default();
        let mut info = TaxonInfo {
            uid,
            ..Default::default()
        };
        if let Some(lang) = &lang {
            if lang == languages[0] {
                let alias: Option<String> = row.get(3)?;
                let common_name: Option<String> = row.get(4)?;
                info.label = label.unwrap_or_default();
                info.alias = alias
                    .unwrap_or_default()
                    .split('\t')
                    .map(|s| s.to_string())
                    .collect();
                info.common_name = common_name
                    .unwrap_or_default()
                    .split('\t')
                    .map(|s| s.to_string())
                    .collect();
            }
            if !default_wiki_link {
                if let Some(name) = row.get(5)? {
                    info.wiki_link = Some(WikiLink {
                        lang: lang.clone(),
                        name,
                    });
                }
            }
        } else {
            info.name = label.unwrap()
        }
        Ok((lang, info, default_wiki_link))
    })?;
    for value in rows {
        let (lang, info, default_wiki_link) = value?;
        if let Some(lang) = lang {
            let i = results.get_mut(&info.uid).unwrap();
            if Some(lang.as_str()) == languages.first().copied() {
                i.label = info.label;
                i.alias = info.alias;
                i.common_name = info.common_name;
            }
            if i.wiki_link.is_none() {
                if default_wiki_link {
                    i.wiki_link = Some(WikiLink {
                        lang,
                        name: i.name.clone(),
                    })
                } else {
                    i.wiki_link = info.wiki_link;
                }
            }
        } else {
            results.insert(info.uid, info);
        }
    }
    let mut stmt = conn.prepare(
        "
        SELECT id, rank, wiki_images, year FROM data
        WHERE id IN (SELECT value FROM rarray(?2))",
    )?;
    let rows = stmt.query_map([&languages[0] as &dyn ToSql, &uids], |row| {
        let uid: usize = row.get(0)?;
        let rank: Option<String> = row.get(1)?;
        let images: Option<String> = row.get(2)?;
        let year: Option<i64> = row.get(3)?;
        Ok((uid, rank, images, year))
    })?;
    for value in rows {
        let (uid, rank, images, year) = value?;
        if let Some(v) = results.get_mut(&uid) {
            v.rank = Rank {
                name: rank.unwrap_or_default(),
            };
            if let Some(images) = images {
                v.images = images
                    .split('\t')
                    .map(|name| TaxonImage {
                        name: name.to_string(),
                    })
                    .collect()
            }
            v.year = year;
        }
    }
    let mut stmt = conn.prepare(
        "SELECT ott, gbif FROM ott_gbif
         WHERE ott IN (SELECT value FROM rarray(?1))",
    )?;
    let rows = stmt.query_map([&uids], |row| {
        let uid: usize = row.get(0)?;
        let gbif: usize = row.get(1)?;
        Ok((uid, gbif))
    })?;
    for value in rows {
        let (uid, gbif) = value?;
        if let Some(v) = results.get_mut(&uid) {
            v.gbif_id = Some(gbif);
        }
    }
    Ok(results)
}

pub fn search_db(sol: &Sol, query: &str, language: &str) -> Result<Vec<usize>> {
    let conn = sol.pool.get()?;
    let suffix_query = format!("{}*", query);
    let mut results = Vec::new();
    let limit = 24;
    let mut stmt = conn.prepare(
        "
SELECT DISTINCT id FROM (
  SELECT rowid, (rank-20) as rank FROM name_search(?1)
  UNION ALL
  SELECT rowid, rank FROM name_search(?2)
) fts
JOIN name n ON n.rowid = fts.rowid
WHERE (language = ?3 OR language IS NULL)
ORDER BY rank LIMIT ?4;",
    )?;
    let rows = stmt.query_map(
        [&query as &dyn ToSql, &suffix_query, &language, &limit],
        |row| row.get(0),
    )?;
    for value in rows {
        let value = value?;
        results.push(value);
    }
    Ok(results)
}

#[derive(Clone)]
pub struct Sol {
    pool: Pool<SqliteConnectionManager>,
}

impl Sol {
    pub fn new() -> std::result::Result<Sol, r2d2::Error> {
        let manager = SqliteConnectionManager::file("db/life.sqlite")
            .with_flags(OpenFlags::SQLITE_OPEN_NO_MUTEX | OpenFlags::SQLITE_OPEN_READ_ONLY)
            .with_init(|conn| rusqlite::vtab::array::load_module(conn));
        let pool = r2d2::Pool::<SqliteConnectionManager>::new(manager)?;
        Ok(Sol { pool })
    }
    // return true if uid and image are a valid combination
    pub fn check_image(&self, uid: usize, image: &str) -> Result<bool> {
        let conn = self.pool.get()?;
        let mut stmt = conn.prepare("SELECT wiki_images FROM  data WHERE id = ?1")?;
        let mut rows = stmt.query_map([uid], |row| {
            let images: Option<String> = row.get(0)?;
            Ok(images)
        })?;
        Ok(if let Some(images) = rows.next() {
            let images = images?;
            images.unwrap_or_default().split('\t').any(|i| i == image)
        } else {
            false
        })
    }
}
