use std::{error::Error, io::Read};

#[derive(Debug)]
pub(super) struct Buffer {
    buffer: Vec<u8>,
    pos: usize,
    len: usize,
    total_read: u64,
}

// the average size of an extracted wikidata json object is 5600 bytes
const START_SIZE: usize = 8192;

impl Buffer {
    pub fn new() -> Self {
        Self {
            buffer: vec![0; START_SIZE],
            pos: 0,
            len: 0,
            total_read: 0,
        }
    }
    // read more data into the buffer
    // give an error if no data can be read
    pub fn read_more(&mut self, input: &mut impl Read) -> Result<(), Box<dyn Error>> {
        // if the buffer is full, make it bigger
        if self.len == self.buffer.len() {
            self.buffer.reserve(self.len);
            self.buffer.resize(self.buffer.capacity(), 0);
        }
        let n = input.read(&mut self.buffer[self.len..])?;
        if n == 0 {
            return Err("Premature end of data.".to_string().into());
        }
        self.total_read += n as u64;
        self.len += n;
        Ok(())
    }
    pub fn total_read(&self) -> u64 {
        self.total_read
    }
    pub fn bytes(&self) -> &[u8] {
        &self.buffer[self.pos..self.len]
    }
    pub fn consume(&mut self, num_bytes: usize) {
        assert!(self.pos + num_bytes <= self.len);
        self.pos += num_bytes;
    }
    pub fn current_value(&self) -> &[u8] {
        &self.buffer[..self.pos]
    }
    pub fn drop_consumed(&mut self) {
        self.len -= self.pos + 1;
        self.buffer.drain(..self.pos + 1);
        self.pos = 0;
    }
    /// place the consumed bytes into a Vec and remove them from the buffer
    pub fn consumed_into_vec(&mut self) -> Vec<u8> {
        self.len -= self.pos + 1;
        let mut vec = self.buffer.split_off(self.pos + 1);
        std::mem::swap(&mut self.buffer, &mut vec);
        vec.truncate(self.pos);
        self.pos = 0;
        if self.buffer.is_empty() {
            self.buffer.reserve(START_SIZE);
            self.buffer.resize(self.buffer.capacity(), 0);
        }
        vec
    }
    #[allow(dead_code)]
    pub fn debug(&self) {
        eprintln!(
            "Buffer {{ bytes: '{}', pos: {}, len: {}, buffer.len: {}}}",
            String::from_utf8(self.bytes().to_vec()).unwrap(),
            self.pos,
            self.len,
            self.buffer.len()
        );
    }
}
