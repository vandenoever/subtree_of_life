server/static/main.css: server/scss/main.scss server/scss/treetable.scss
	scss $< $@

db/wikidata_json.sqlite: latest-all.json.bz2 manual_links.csv
	lbzcat latest-all.json.bz2 | cargo run --release --bin sol parse
