# This file contains manual additions to the table ott_id
# Use it like this:
# < manual_links.sql sqlite3 db/life.sqlite

CREATE TEMP TABLE import(id, ott_id, comment);
.separator ,
.import manual_links.csv import

INSERT INTO ott_id SELECT i.id, i.ott_id FROM import AS i LEFT JOIN ott_id o ON i.id = o.id WHERE o.id IS NULL;
