# Instructions to build the database for SOL

This document explains how `life.sqlite` is created.

SOL runs from two of Sqlite databases. `life.sqlite` holds the taxaonomy and additional information. `image_cache.sqlite` is a cache for images from Wikipedia.

`life.sqlite` is a readonly database. `image_cache.sqlite` starts out empty and is filled with images that are requested by visitors.

## Preparation

### Compiling the software

`sol` is an executable for processing the source data. It has to be compiled with `cargo build --release --bin sol`.

## Retrieving the source data

SOL uses information from four sources.

TODO: make this into links to the files and add commands to uncompress the files

 - `latest-all.json.bz2` from WikiData,
 - `taxonomy.tsv` from the Open Tree of Life project,
 - `manual_links.csv` from this project,
 - `Taxon.tsv` from the Catalogue of Life.

### latest-all.json.bz2

WikiData publishes their database as large files. The recommended format to use is JSON. `latest-all.json.bz2` is a file published by WikiData. It is a 72GB compressed JSON file. The uncompressed size is about 1.5 TB.

``latest-all.json.bz2`` contains information about all entities in WikiData. The data in this file is filtered to contain only the entries related to taxonomies. This is done with the command:

```bash
lbzcat latest-all.json.bz2 | sol parse
```

The entries are recognized by their properties. Entities with properties related to taxonomies are stored in a database called `wikidata_json.sqlite`. This file will be large: approximately 30GB.

Some entities are not found in this way. These need to be specified manuall in the file `manual_links.csv`. That file is read by `sol parse` as well.

The command `sol parse` can take one to three hours to run.

### taxonomy.tsv

```bash
< ott3.3/taxonomy.tsv cargo run --release --bin sol parse_ott
```

