
https://tree.opentreeoflife.org/about/taxonomy-version/ott3.3
http://files.opentreeoflife.org/ott/ott3.3/ott3.3.tgz
sha256sum e9ddac197ba74a2f57d1097aab51c112881fb24041617ed6f89521fa570bdc94

GBIF Backbone Taxonomy
https://www.gbif.org/dataset/d7dddbf4-2cf0-4f39-9b2a-bb099caae36c

Catalogue of Life
https://download.checklistbank.org/col/
