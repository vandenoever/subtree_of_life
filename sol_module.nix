sol:
{ config, lib, pkgs, ... }:
let cfg = config.services.sol;

in with lib; {
  options = {
    services.sol = {
      enable = mkEnableOption "Run a Subtree of Life server";

      user = mkOption {
        type = types.str;
        default = "sol";
        description = "User that runs sol.";
      };

      group = mkOption {
        type = types.str;
        default = "sol";
        description = "Group that runs sol.";
      };

      home = mkOption {
        type = types.str;
        default = "/home/sol";
        description = "Home directory of the sol instance.";
      };

      listenAddress = mkOption {
        type = types.str;
        default = "127.0.0.1";
        description = "Address to listen on.";
      };

      listenPort = mkOption {
        type = types.int;
        default = 8001;
        description = "Port to listen on.";
      };
    };
  };
  config = mkIf cfg.enable {
    systemd.services.sol = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      description = "Start the sol service.";
      path = [ ];
      environment = { RUST_BACKTRACE = "full"; };
      serviceConfig = {
        User = cfg.user;
        Group = cfg.group;
        ExecStart = "${sol}/bin/server --bind ${cfg.listenAddress} --port ${
            toString cfg.listenPort
          }";
        WorkingDirectory = cfg.home;
        Restart = "on-failure";
        RestartSec = "3";
      };
      unitConfig = {
        StartLimitIntervalSec = 30;
        StartLimitBurst = 3;
      };
      preStart = "";
    };

    environment.systemPackages = [ sol ];
    users = {
      users.${cfg.user} = {
        isSystemUser = true;
        description = "sol daemon user";
        home = cfg.home;
        createHome = true;
        group = cfg.group;
      };
      groups.${cfg.group} = { };
    };
  };
}
