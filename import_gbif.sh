#! /usr/bin/env bash

# Select columns and skip the first line
cut gbif/backbone/Taxon.tsv -f 1,12,15 | tail +2 > Taxon.tsv

cat << EOF | sqlite3 -batch -bail -echo db/life.sqlite
CREATE TABLE gbif (
    gbif   INTEGER PRIMARY KEY,
    rank   TEXT NOT NULL,
    status TEXT
);
.mode csv
.separator "\t"
.import Taxon.tsv gbif
EOF

rm Taxon.tsv
