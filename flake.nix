{
  description = "Subtree of Life";
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = naersk.lib."${system}";
        sqlite = pkgs.sqlite-interactive;
        inputs = with pkgs; [ graphviz pkg-config sqlite openssl ];
      in rec {
        # `nix build`
        packages.sol = naersk-lib.buildPackage {
          name = "sol";
          root = ./.;
          nativeBuildInputs = inputs;
          doCheck = true;
          doInstallCheck = true;
          postInstall = ''
            mkdir -p $out/share
            cp -a i18n server/static $out/share
            "${pkgs.sass}/bin/scss" server/scss/main.scss $out/share/static/main.css
          '';
        };
        packages.default = packages.sol;
        nixosModules.default = import ./sol_module.nix packages.sol;

        # `nix run`
        apps.sol = utils.lib.mkApp {
          name = "sol";
          drv = packages.sol;
        };
        apps.server = utils.lib.mkApp {
          name = "server";
          drv = packages.sol;
        };
        apps.default = apps.sol;

        # `nix develop`
        devShells.default = pkgs.mkShell {
          nativeBuildInputs = with pkgs;
            [
              rustc
              cargo
              clippy
              cargo-audit
              cargo-watch
              rust-analyzer
              rustfmt
              cargo-tarpaulin
              sass
              openssl
              lbzip2
              sqlite-analyzer
              bacon
            ] ++ inputs;
        };
      }) // {
        # a container for testing
        nixosConfigurations.container = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            self.nixosModules."x86_64-linux".default
            ({ config, pkgs, ... }: {
              boot.isContainer = true;
              networking.hostName = "sol-test-container";
              networking.firewall.allowedTCPPorts =
                [ config.services.sol.listenPort ];
              services.sol = {
                enable = true;
                listenAddress = "0.0.0.0";
                listenPort = 9123;
              };
              system.stateVersion = "23.11";
            })
          ];
        };
      };
}

