Search = Zoek
add-this-taxon = voeg dit taxon toe
remove-this-taxon = verwijder dit taxon
no-results = Geen resultaten.

domain = domein
kingdom = rijk
phylum = stam
subphylum = onderstam
superclass = superklasse
class = klasse
subclass = onderklasse
infraclass = infraklasse
superorder = superorde
order = orde
suborder = onderklasse
infraorder = infraorde
parvorder = parvorde
superfamily = superfamilie
family = familie
subfamily = onderfamilie
tribe = tak
genus = geslacht
species = soort
subspecies = ondersoort
varietas = variëteit
variety = variëteit

# billion, million and kilo years ago
bya = Ga
mya = Ma
kya = kya
bya-long = miljard jaar geleden
mya-long = miljoen jaar geleden
kya-long = duizend jaar geleden
